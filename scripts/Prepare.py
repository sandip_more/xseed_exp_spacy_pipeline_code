import re
import datetime
from datetime import date
import docx2txt
import glob
import os
import pandas as pd

# Get dvc remote url and read data
import dvc.api

cmpny_path = r"assets\Company Data.xlsx"
job_path = r"assets\job role.xlsx"
repo = r"."
version = "v1"

cmpny_url = dvc.api.get_url(
    path=cmpny_path,
    repo=repo,
    rev=version
)
job_url = dvc.api.get_url(
    path=job_path,
    repo=repo,
    rev=version
)

# Import pandas


## Load the xlsx file
cmpny_excel_data = pd.read_excel(cmpny_url)
# Read the values of the file in the dataframe
data = pd.DataFrame(cmpny_excel_data)
## Print the content
# print(data)

companys = []
for row in data.itertuples():
    b = (row[1].strip()+' '+row[2].strip())
    b = b.strip()
    companys.append(b)



# Load the xlsx file
job_excel_data = pd.read_excel(job_url)

# Read the values of the file in the dataframe
role_data = pd.DataFrame(job_excel_data)
role = []
for row in role_data.itertuples():
    u_l = (row[1])
    role.append(u_l)

job_role = []
for i in role:
    # for ignore (text)
    plain_edu = re.sub("[\(\[].*?[\)\]]", "", i)
    # \xa0 is unwanted space char. which is appears when we pest data
    plain_edu = plain_edu.replace('\xa0', ' ')
    plain_edu = plain_edu.strip()
    plain_edu = plain_edu.replace("  ", " ")
    job_role.append(plain_edu)
job_role = list(set(job_role))

garbage_data_1 = ['EDUCATIONAL DOCUMENTS MASTERS IN COMPUTER SCIENCE. MARSHALL UNIVERISTY, HUNTINGTON, WEST VIRGINA.',
                  '6-May 2017. MASTERS IN MOBILE AND UBQUTIOUS COMPUTING, TRINITY COLLEGE DIUBLIN, DUBLIN-02, IRELAND.',
                  'aScript, HTML, Log4j, Oracle 10g, Apache POI, Caster, XMI.',
                  'DAO, VO Patterns, Tomcat 5.0, SQL, Oracle 9i, Linux.',
                  'Observer, Decorator, Front Controller and DAO pattern.',
                  'JUnit, JAX-B, WebSphere, JIRA, Maven, GIT, RAD, Jenkins, MongoDB, Cassandra.',
                  'SQL, MySQL Server, Google cloud platform, JIRA, RMI, IntelliJ, IDE, LINUX.',
                  'IBM WebSphere, Maven, Clear Case, SoapUI, Oracle 11g, Spring JMS, IBM MQ.',
                  'Junit, Ant, JBOSS, Rally, Apache Axis, Web Service, SOAP, WSDL.',
                  'SQL-Server, Apache Tomcat, Oracle, PL/SQL, CVS, PVCS, Junit, Windows.',
                  'deloping marketing plan and objectives and identifying logistics needs.',
                  'getting resolved in the appropriate time frames.',
                  'prasentation, throughout all Gulfstream locations in the US.',
                  '▪ Led cross-functional and virtual teams and meetings.',
                  'FDA’s regulations – CFR 21 Part 11, cGMP',
                  'HR & FI/CO, SAP PI, FIORI, Web Services, JIRA & Blue works',
                  'SAP ECC, Web Services, Doors, MS Project, HP ALM, QTP, and LoadRunner',
                  'SAP ECC, QTP, SD, MM, FI/CO, Windows XP, Ms Projects & Visio',
                  'Reports, Toad, Ultra Edit Other Companies',
                  'professional, Citrix Metaframe Server, Unix',
                  'test scripts via Selenium Webdriver, TestNG, Maven tools.',
                  'tools and Web Services are being used for MD3 Project.',
                  'are being used for CS and CMS portfolios.',
                  'Cunsultant /QA Project Lead – Agile/Scrum/PM/QA , Kmart Point of Sale',
                  'project updates to Project Manager and Senior Manager.',
                  'ct/QA Lead – Agile/Scrum/PM/QA , Point of Sale (Nextgen POS), GMAC',
                  'on tools for daily build. Bank of New York Mellon / Workbench Applications, Web Inquire, GSM',
                  'in imparting KT to onsite from offshore. Directorate of General of Artillery, DV, AMS & MIS',
                  'shakeout testing to ensure adherence to testing practice.',
                  'test plans for System Integration Testing and User Acceptance Testing.',
                  'Eclipse Databases MySQL, Oracle, MS Access',
                  'Informatica Power Center 9.6, Tableau 8.3, SharePoint, HP Quality Center 11',
                  'MS Project, MS VISIO, SharePoint, Rational Requisite Pro, HPQC',
                  'Balsamiq, Visio, MS Project, MS SharePoint, HPQC, Windows, JIRA, Selenium, SOAP UI.',
                  'Windows XP, MS Project 2010, SharePoint, Tableau 6, HPALM 11.0, HTML, CSS, Java.',
                  'Eclipse Databases MySQL, Oracle, MS Access',
                  'Informatica Power Center 9.6, Tableau 8.3, MS Office, HP Quality Center 11',
                  'Methodologies Agile, Waterfall, Test Driven Development',
                  'Log4J, Oracle, Kibana, Splunk, Selenium, Mongo DB, WSD and Spring Tool Suite.',
                  'Log4j, Maven, Web Sphere Application Server, Jenkins and Spring Tool Suite (STS).',
                  'Jenkins, Gradle, GIT, Eclipse, JIRA, Agile Methodology, Windows, Linux, Rational Rose.',
                  'IRA, Log4J, Eclipse, SVN, Sybase, RCP and Web Logic Server.',
                  'Oracle, ANT, Eclipse IDE, JUNIT, JIRA, Tortoise SVN, and UNIX.',
                  'Computer Science & Technology, Mumbai University',
                  'AVA, XML, HTML5, Bootstrap, AJAX, Jquery, Angular JS, Eclipse EDE, Jenekins.',
                  'Architecture, Win-FTP, Java servlets V3.0, JSP V2.2, Java EE 7.',
                  'Up Screens V4.32, Microsoft SQL Server 2012, Tableau, MS SharePoint',
                  'Mercury Quality Center, MS Visio, MS Project, Windows XP/2000',
                  'Environment: Waterfall, MS Office, SQL, UAT Testing, HP QC, MS Visio',
                  'MS Visio, MS Project, Justin Mind, My SQL, Java, HTML, HPQC.',
                  'QuickBooks, MS Dynamics GP, MS Dynamic AX, Oracle financial suite.',
                  'boarding efforts. Foster a strong team environment and help subordinates as needed.',
                  'AX Dynamics. Familiarity working with XML and SQL SPs (stored procedures).',
                  'as a point of contact for investors, clients, media and prospective clients. Graduate Assistant',
                  'international students regarding admission, financial aid, housing, and insurance.',
                  'to get client feedback and response.',
                  'Michigan State University, GPA– 3.56',
                  '(UAT) sessions. Environment: MS Visio, DOORS',
                  'Test Director, MMIS, MS Access, HTML, XML, Java Script, Java, ASP, DB2.',
                  'MS Excel, MS Project, MS Visio, BRDs, FRDs, Quality Assurance',
                  'UM, MS Visio, MS Project, Axure, SQL, My SQL Workbench.',
                  'Tools ANT, Maven, Gradle, Groovy, Log4j.',
                  'SVN, GIT, log4j, Junit, Selenium, JMS Queues, WebSphere.',
                  'Maven, Apache Camel, Junit, Mockito, Jenkins, JMS Queues.',
                  'JMS, Maven, Jenkins, Apache Camel, SVN, JIRA.',
                  'UML, Eclipse, Junit, Maven, Agile, SOA, JIRA, JENKINS, Selenium.',
                  'SOAP UI, AJAX-WS RAD, Log4j, SQL, Maven, Tomcat, Log4j, WebLogic.',
                  'scrum, Integration tool, Log4J, GIT, JUnit.',
                  'Ajax, Java Beans,AWS,Jboss,JDBC, iBatis 2.2,Spring 4.0, SOAP, SOAP UI, Web services,LDAP,ATG 9.',
                  'Selenium, JIRA, Apache Tomcat, JBOSS,TDD, Jasmine and Karma.',
                  '11g, UNIX Shell Scripting, Putty, JUnit, Log4J, JMeter, JProfiler.',
                  'EDUCATIONAL DOCUMENTS MASTERS IN COMPUTER SCIENCE. MARSHALL UNIVERISTY, HUNTINGTON, WEST VIRGINA.',
                  '6-May 2017. MASTERS IN MOBILE AND UBQUTIOUS COMPUTING, TRINITY COLLEGE DIUBLIN, DUBLIN-02, IRELAND.',
                  'of the work items which were later developed by the development team',
                  'and automotive markets. Project Description: SDL Tridion Technical Services – Website revamp',
                  'of the public healthcare sector in the long run.',
                  'for the project Production Release & Stabilization',
                  'lso the Venue Operations Partner) Project Description: Support & Maintenance of BIS Applications – (',
                  'of Cost/Revenue for the project Production Release & Stabilization',
                  'than 40 offices. Project Description: Support for Insight (financial reporting portal) system',
                  'giving them proper transition of knowledge and all documentation.',
                  'Project Description: Implementation of reporting environment for Internal Finance Department',
                  'from the development environment to the production environment',
                  'is static. Project Description: Implementation of reporting environment for International Markets',
                  'Central Management Console. Project Description: Implementation of Portfolio Risk Manager Module',
                  'proper transition of knowledge and all documentation. Project Description: Run the Business (RTB)',
                  'other developers. Project Description: Implementing Oracle Suites (AR, AP, GL) in UK and Ireland',
                  'and that of other developers Project Description: HRCC BO Globalization Implementation Phase I',
                  'the client. Unit testing own code and that of other developers Project Description: BO XI Migration',
                  'own code and that of other developers Project Description: Top 10 Customers and DnBI Pricing',
                  'Safe to maintain code and documentation versions Project Description: Preferred Pricing Phase-2',
                  'Unit testing own code and that of other developers Project Description: Preferred Pricing Phase-1',
                  '6.0/6.1, Websphere Commerce Server 6.0, Apache 2.0.47, IHS 6.0/6.1',
                  'Clear Quest, SVN, Systemware.',
                  'Services, XML, Java, Java Script, HTML, Stored Procedures, Mainframes, .Net',
                  'Technology - Java, Java Script, Mainframes, Remote Procedure Calls',
                  'XML, Java, Java Script, HTML, Mainframes',
                  'of Electronics and communication Engineering, JNTUH, INDIA',
                  'Server 2014, maven, Java Batch, Java 8.0.',
                  'Scala, HTML5, JSP, Spark, Swagger, HDFS, AngularJS 3.',
                  'Maven, Eclipse, JAX-B, JAX-RS/Jersey, JAX-WS, XML and JSON.',
                  'XML, AngularJS, Oracle RDBMS and JSON.',
                  'AP, Maven, log4j, Junit, XML, Web Services.',
                  'project, MS Visio, Microsoft Office',
                  'product/approach recommendation and business use case validations',
                  'ClaimCenter V5.0. It includes claims application interfaces, Reporting and Configuration',
                  'and release accordingly Compliance to schedule and containment of defect leakage',
                  'compliance during Supply Chain training.',
                  'SharePoint, MS Project, MS Visio, SQL, SOAP, XML, MMIS, Agile.',
                  'Visio, Windows Vista, Mercury Test-Director.',
                  'HTML, CSS, SASS, AJAX, Jira, Angular JS, NPM/BOWER.',
                  'CXF, GIT, UNIX, AGILE-SCRUM, Oracle, Tomcat, Drools.',
                  'Drools, JUnit, Mockito, Maven, Web services, DAO, Web Sphere.',
                  'RAD , IBM MQ Series, Rest, SQL Server, Drools, Unix, UML.',
                  'Struts, Java, Web logic, XML, IBATIS, JSP, JBOSS, ANT, Eclipse, Oracle.',
                  'SQL Server Services, Cognos BI, Informatica, OLAP, OLTP, Data Warehouse.',
                  'Windows (98/XP/7/Vista/8), MS Office Suite, Jira, , SQL.',
                  'MS Visio, UML, SQL Server, SQL, MS Office and Windows 2000',
                  "University with Distinction - 2001",
                  'Experienced in HP ALM for defect tracking',
                  'Review activities Participated in Bug fixing activities',
                  'testing with stakeholders and business users',
                  'Putty, Tableau, Teradata, SQL Assistant SAS, FLOAD, MLOAD, UNIX, SQL, JIRA.',
                  'MS Visio, MS Project, HTML, HP ALM/Quality Center, Oracle, SQL Server, ClearQuest.',
                  'MS Access, MS Project, SharePoint, PDMS, MS Visio, MS Office, IBM DOORS, JIRA.',
                  'Rose, UML, Use Case, Activity/ State Diagram, MS Visio, Oracle, Quality Center.',
                  'Visio, MS Word, MS Excel, Requisite Pro, JIRA, and Crystal Reports',
                  'and requirements. Isolate, replicate and reported defects and verify defect fixes.',
                  'MS Visio, Windows XP, Quality Center, Clear Quest, JIRA.',
                  'and reporting scripts in Oracle SQL, PL/SQL, JavaScript and HTML.',
                  'Business requirements, functional requirements and Use case diagrams.',
                  'to gather detailed information about the product being built.',
                  'ofessional, MS Project Server, JIRA, RALLY TECHNICAL SKILLS: PROFESSIONAL EXPERIENCE: Citi Bank, NJ ',
                  'Logged critical defects and was appreciated by the UAT Program team.',
                  ' Production Support and Maintain release for builds created.',
                  'internal and external workflow. Involved in System test in the URL',
                  'Providing Transition to Support knowledge to support team.',
                  'MS Visio, MS Office Suite, MS Outlook',
                  'Rally, Restful web services, UNIX, JAVA, MS Outlook',
                  'HTML, Java, Informatica for ETL and ERwin for Data modeling',
                  'FAST ESP ,TeraData and Sql server.',

                  'Engineering, University of JNTU, Hyderabad, AP',
                  'OneOps, Gray Log, Apache CXF, Node JS, Mocha, Oracle, Maven, GIT, Jenkins, Jira Azuba',
                  'Cloud Netflix, Zuul, Microservices, Hibernate, Mongo dB, MySQL',
                  'Restful Web Services, JDK 1.7, UNIX, AWS, S3, Log 4j, and Maven.',
                  'pplication Server, JDK 1.5, UNIX, AIX, Solaris, Log 4j, and Ant.',
                  'Servlets, ORACLE 11g, WebSphere Application Server, JDK 1.5',
                  '11g, WebSphere Application Server, JDK 1.5, UNIX, Solaris, Log 4j, and Ant.',
                  'Eineering at JNTU, Kakinada, Andhra Pradesh, India.',
                  'Apache Airflow Kafka, Zookeeper, Sqoop, Cassandra, Talend, Splunk, HBase.',
                  'Talend, Pig, Linux, Java, Scala, Python, Ambari, Zookeeper.',
                  'Sqoop 1.4.2, Flume 1.3.1, Eclipse, AWS EC2, and Cloudera CDH 4.',
                  'Hive, Map Reduce, Core Java, Pig, Sqoop, Cloudera CDH4, Oracle, MySQL.',

                  'VS, Junit, UNIX, Log4J, CSS Style Sheets, Apache Tomcat, J2EE, Maven 3',
                  'HTML, Oracle Web logic server, Eclipse, MySQL, JUnit.',

                  'General Management And Communication Skills Course from New Delhi by ICAI. EXPERIENCE WITH GENPACT',
                  'time resolution of various issues that come up while testing.',
                  'process Playback sessions to the business & December 2021 ERP Support team',
                  'Invoices, credit memos, Debit memos, and Prepayment invoices.',
                  'issues for I Expense, Finance modules with the Team',
                  'all the documents for internal and external audit.',
                  'that no transactions lie piling up even in the Interface tables.',
                  'data (Financial/metrics) benchmark tools.',
                  'projects and performed other duties as assigned.',
                  'and conversions Micro Village Communications.',
                  'Leader Bronze by Toastmasters International',
                  'HTML, Java, XML, Rational ClearQuest, Windows 7, AWS, MMIS.',
                  'Suite (RequisitePro, ClearQuest),Business Objects, Windows XP.',
                  '9i, SMTP, Unix, Web Client, EJB',
                  'Microsoft Office tools, MS Project, SQL',
                  'PL/SQL, JIRA, BALSAMIQ, AGILE, SCRUM, IOS, Healthcare, HIPAA, HITECH',
                  'PHP, MySQL, JAVA, HTML5, CSS, AGILE, JIRA, IOS, SKETCH',
                  'MS VISIO, AGILE, SCRUM, JIRA, Healthcare, HIPAA, HITECH',
                  'CSS, PENCIL, MS VISIO, HEALTHCARE, HIPAA, HITECH',
                  'Masters in Business Administration(MBA), Madurai Kamaraj University (Madurai, India)',
                  'ems Windows, Unix, Linux and MAC.',
                  'Apache JMeter, JQuery, Mule ESB JUnit, Maven, EMR.',
                  'SVN, Apache Maven, JUnit, HTML Unit, XSLT, HTML/DHTML.',
                  'Rational Rose, Windows XP, CVS, JUnit, ANT, Adobe Flex, Visio.',
                  'Windows XP, REST API, NetBeans, UNIX.',
                  'Application Server, Eclipse, Oracle.',
                  '| VB Script | JavaScript | SQL server',
                  'ensured 99.9% system uptime in production.',
                  'meetings in conjunction with the Business team.',
                  'help hints provided by the application.',
                  'test backend services including Façade and Ecommerce.',
                  'Sustenance Engineering (Windows Vista Avalon and DirectX team)',
                  'Selenium (Environment), Agile, UML , JSP, Xml, SOA',
                  'VLET, MAVEN, Xml, SOA, JMS, Apache Tomcat, Linux, PHP',
                  'C, C++, Mainframe, COBOL, PL/I, JCL, SQL, SAS VB',
                  'Selenium, ANT, EMC Documentum, JIRA, SWING, CVS, Waterfall Model.',
                  'Selenium (Environment), Agile, UML , JSP, Xml',
                  'VLET, MAVEN, Xml, SOA, JMS, Apache Tomcat, Linux, PHP',
                  'C, C++, Mainframe, COBOL, PL/I, JCL, SQL, SAS VB',
                  'Selenium, ANT, EMC Documentum, JIRA, SWING, CVS, Waterfall Model.',
                  'meets the system Requirement.',
                  'SQL, SOA Infra, XML, SpecBuilder, MMIS, Mainframe.',
                  'Java, XML, Mainframe and Rational ClearQuest, ClearCase, Windows XP.',
                  'vista/Windows 7, Mercury Test-Director, MS Visio, Business Objects.',
                  'AWS, tortoise SVN, Mercury Quality center, BEA WebLogic Application Server 9.2.',
                  'DBC,Oracle10g, Subversion, Git, Glassfish Server 3.1.2,Toad,AJAX.',
                  'Server 6.1, TCPMon, Soap UI, Visio, Rational Rose, Log4j.',
                  'Incident, Change and Escalation Management.',
                  'Maven, Windows 2008, Windows 10, Web applications, Open Project, MS Office',
                  'JAVA, SOAP UI, Windows 2008, Macintosh, Web applications, Jira, MS Office suite',
                  'Maven, Windows 2008, Windows 7, Web applications, Bugzilla, MS Office suite Genpact',
                  'Infosys Management on weekly basis.',
                  'with Service Level Agreements.',
                  'solve production tickets through BMC Remedy tool.',
                  'DB2, Docker, Maven, Jenkins, Log4J, JUnit, Mockito',
                  'AWS (SNS, SQS, EC2, Cloud Watch), Oracle 11g, SQL Server.',
                  '2.5.1, Cassandra, SOAP, Rest, WSDL, Windows 7, Eclipse Luna.',
                  'SQL, Oracle, Hibernate, Toad, Log4j, CVS, WebSphere, Windows, UNIX.',
                  'and help them to close new accounts (Proposals and SOW’s)',
                  'Managed, planned and allocated test resources for projects.',
                  'testing process for onsite-offshore delivery model.',
                  'off the Quality Gates (Delivery to the customer)',
                  'testing been closed before Carrying out system testing.',
                  'HTML, XML, MS Access, ORACLE, MS Word, Excel, Quality Center, Sharepoint',
                  'Services – Axis 2.0, DB2, Windows XP, Unix, Selenium, Quality Center.',
                  'Heads Down & PEPs, MS Access, ORACLE, MS Word, Excel, PowerPoint.',
                  'Unified Process (RUP), DB2, Rational Test Manager, Win Runner, HTML, XML',
                  'development process to check the functionality of the software',
                  '11, AIX (UNIX) 7.1, Windows Server 2008 R2.',
                  'Backbone JS, PL/SQL developer, iText, Apache POI, Microservices.',
                  'Ajax, JUnit, Ant, Eclipse 3.3, IBM Web Sphere 6.1, DB2, subversion, Linux.',
                  'scripts, Control M, SQL Developer, Oracle Virtual Box, Rally, Blaze.',
                  'spring 3, JMS, JavaScript, Oracle 10g, JUnit, JDBC, ANT and Microservices.',
                  'Application Server 6.1, Web services, JIRA, Junit, RAD7.',
                  'Batch, Hibernate, Cassandra, Docker, Ant, Coherence, Rally, Apache Camel.',
                  'kins, Cassandra , Couchbase , Hazelcast , Mule API Gateway , Cucumber JVM.',
                  'JUnit, jQuery UI , CSS, JavaScript, Unix and Oracle 11g, Bootstrap Framework.',
                  'Application Server , MySQL, Eclipse, Log4j, IBM ClearCase, etc.',
                  'Oracle 10g, Web Sphere, Ant, RAD, Eclipse, UNIX, Perl.',
                  'Log4j, Eclipse, Oracle 11g, SQL, Selenium.',
                  'SOAP, agile and Windows, SQL, Node JS, Tomcat.',
                  'JSP, Servlets, MVC, J2EE, Karma, Angular JS1. x.',
                  'Bootstrap, JSON, DTD Restful API, AJAX, Servlet.',
                  'Windows 7/Vista/XP/8, Unix, Linux Agile',
                  'Spring Boot, Spring Cloud, Jenkins,Rest & SOAP Web Services, Big bucket.',
                  'jenkins, Rest & SOAP Web Services, JUnit Mockito, ANT.',
                  'with Elastic Search Engine, Ready API, Jira, Jasper Soft and Jasper Server',
                  'Node Js, Backbone Js, Web services, HTML, CSS, JavaScript, JSP, Splunk, Grunt',
                  'NO/SQL Micro services.',
                  'Node JS, Oracle, Splunk, My SQL.',
                  'SOAP UI, JAX_RS, JERSEY, Windows XP.',
                  'Java Script, SOAP 2.0 Restful Services, Node JS Git-Hub.',
                  'Restful Services, Angular2.0, NodeJS Active MQ, Git Hub.',
                  'WebSphere Application Server, SOAP 1.2, RestFul Services.',
                  'JS, JavaScript, HTML5, CSS, JQuery 1.4, EJB 3.0, Nodejs.',
                  'SQL, PL/SQL, Maven, Eclipse, Log4j, JUnit, GIT, Docker, AWS EC2, JIRA.',
                  'Log4J, SVN, UML, JAXB, XML, Scrum, Unix/Linux, JUnit.',
                  'XML, XSD, XSLT, Oracle, Linux, JBoss, Log4J, JUnit, ANT, CVS',
                  'clipse, SOAP, Apache Tomcat, Oracle 10g, LOG4J, SVN.',
                  'variances Coordination with Production Support (Operations), Change and Release Management groups',
                  'Design Process, ITIL Transition Process to one of the clients',
                  '% by tactful development of SOW and negotiation with vendors Instrumental in development of budgets',
                  'with client’s need. Defined and implemented the Vendor Management work flow process for the client',
                  'to implement ITIL Service support and training Early Career',
                  'SVN, JMeter, UNIX. Project Title: Car Rental',
                  'Apache Camel, JBoss Fuse 6.2, Active MQ, Apache ATOM, WESB',
                  'mangoDB, JUnit, Html, Unit, XSLT, HTML/DHTML, JENKINS, Spark, Kafka.',
                  'Application Server, Tomcat, Oracle DB, MQ Series, JUnit, UNIX.',
                  '8.0, Oracle 9i RDBMS, Mercury Quality Center 9.0, Eclipse 3.2.',
                  'JUnit, Weblogic10.0, Oracle 11g, Wsdl, IBM Rational rose, Maven, Eclipse, XSD.',
                  'Big Data Ecosystem HBase, Hadoop MapReduce, Spark',
                  'Communicating with different web Services by using Spring Integration.',
                  'development on Windows and UNIX platforms using MVC pattern.',
                  "Written Maven Scripts to build generate WAR file and deploy the application.",
                  'hands on in Full stack JAVA/J2EE, Angular, TypeScript, JavaScript, Html5, jQuery.',
                  'AngularJS/jQuery, HttpClient. Enriched the system by implementing Solr and big data search.',
                  'framework, web service client, JUnit test, jQurey, HTML/JavaScript/CSS and application software.',
                  'Hibernate, Html, xml, Javascript, Ajax, jQuery, JBoss, Eclipse, Ant, SQL Server, JDK, SVN, etc.',
                  'JDBC, Oc4j, EJB, Html, xml, Javascript, Ajax, JUNIT as well as JBoss, Eclipse, Ant, JDK, SV, etc.',
                  'CSCP, LSSGB (Lean Six Sigma Green Belt)',
                  'Issue and Risks and Confluence site for managing project documents.',
                  'request and status of integration between ecommerce portal and SAP system.',
                  'BSA & SAP team to execute those requirements and configure SAP system.',
                  'all IT work in Retail stores and back office for sales and Inventory.',
                  'process for store inventory replenishment orders by engaging 3PL.',
                  'and data integration between various systems.',
                  'tool, Google Maps API, Drools, iLog, Linux, Micro services etc.',
                  'Ant, HP QC, Coherence, Rally, Apache Camel.',
                  'XML, Servlets, Design Patterns, Web Services.',
                  'Matrices (RTM). Education: Master of Science in Engineering – New Jersey Institute of Technology',
                  '(IT Enablement Center) PPMC (Project & Portfolio Management Center).',
                  'dewire policy admin system, CAAS, Yacht, PLS, ORCA, DRC, SharePoint.',
                  '(F14), CaliberRM 64Bit Production, Optimal Trace, MagicDraw UML.',
                  'Rose, Bugzilla, Snagit, Adobe Acrobat Professional, HP Quality Center.',
                  'HTML 5, , Ant, Windows, GIT, Log4j.',
                  '11g, Jenkins, Angular JS, Javascript, NodeJS..',
                  'Sphere 6.1, DB2, subversion, Linux.',
                  'JavaScript 1.2, XML/XSLT, SAX, DOM.',
                  'Mock, ETL, UML, JPA, Jenkins, apache',
                  'Bachelor of Computer Application, Gandhinagar, Gujarat, India',
                  'Grunt, Tortoise SVN, Putty, LAMP, Visio, Team track, Quality Center.',
                  'Web Services, Rest API, Junit, Websphere, Oracle, AWS, log4j',
                  'VN, Rest web services , maven, Eclipse Kepler , java script , Xml , mysql.',
                  'Scripts, Agile (Scrum), Rational Rose, JUnit, Log4j',
                  'Studio Team Services, Visual Studio Code.',
                  'Enterprise edition, Outlook 365, Oracle applications.',
                  'Warehouse, Tableau, UML, and UAT Testing.',
                  'Microsoft Server Integration Services.',
                  '.net, SOA, SOAP, XML, MS Visio 2008), HTML, HPQC',
                  'Markup Java Script, JQuery, AJAX, XML, HTML',
                  'App Server8.5 , Modular design patterns, STS',
                  '6.0, Modular design patterns, STS.',
                  'ODM,Message Brokers Tool Kit, Star Team.',
                  'developer v6.0.1,Message Brokers Tool Kit, Star Team.',
                  'tool Kit, Rational Software Architecture v6',
                  'Windows XP/VISTA, 4GL, UNIX Scripts, Sql, putty, 4GL',
                  'Java Technology Version 1.4, MySql,DB2, DB2,MySql',
                  'ESB, SOA, Guidewire Suit, XML, WSDL, SOQL.',
                  '9.5, MS SQL, POSTGRE SQL, Main frames.',
                  'Data Loader, Workflow Approvals.',
                  'framework, POSTGRE SQL, Apache Tomcat, Outlook, CRM Systems.',
                  'HTML, CSS, JS, MS Project, MS Visio, UML, Test, SQL version 2012.',
                  'in solving performance issues and bug fixes.',
                  'based on the Use Cases and Business Rules document.',
                  'Team Foundation Server EDUCATION Bachelor of Technology - Jawaharlal Nehru Technological University',
                  'Nehru Technological University August 2008-May 2012 Master of Science -University of New Haven',
                  'MS PowerPoint, MySQL Workbench.',
                  'Guidewire Suit, XML, Tableau, WSDL, SOQL.',
                  '9.5, MS SQL, POSTGRE SQL, Main frames.',
                  'Agile methodology,Log4j, MVC Patterns, Oracle 11G',
                  'Oracle 11G, SUN Solaris, WebLogic.',
                  'Xml, ATG, Java Mail API, SQL server',
                  'Web Services, Windows XP.',
                  'Cisco Certified Network Associate (CCNA)',
                  'Plan, MS Office, VB, HTML, JavaScript, MS Visio, SQL',
                  'Project Plan, MS Office, VB, HTML, JavaScript, MS Visio, SQL',
                  'project, MS Office - 2013, Rational Suite, SharePoint 2013.',
                  'MS Excel, Oracle, Sybase, SharePoint, and MS Visio, and Office.',
                  'SQL – 2008, Visio, HP ALM 11.0, loadrunner, MS Office – 2010.',
                  '8, HP ALM, MS Office, Visio and Project.',
                  '(EOB), In & Out of network benefits',
                  'Suite, Quality center, RUP & UML, Share Point, Medicare, Medicaid.',
                  'Office tools, MS Project & MS Visio, Facets 4.81 & Facets interfaces (IFOX).',
                  'MS Office Suite, GAP Analysis & UAT & Cognos.',
                  'Server, Mockito, JMS, UNIX, Jenkins, JIRA, log4j and GIT.',
                  'Kafka, Elasticsearch, Logstash, JIRA, Jenkins, JMeter.',
                  'SQL, Cassandra, JIRA, QA, SVN, Junit, JBOSS Server, Drools.',
                  '4J, Eclipse, SVN, Sybase, RCP and Web Logic Server.',
                  'HTML, CSS, JavaScript, UNIX, CVS, DB2 and Ionic Framework.',
                  'Maven, JUnit, Log4j, CVS, SVN, GIT, Source Tree, Jenkins Operating Systems Windows-10/8.1/7/XP/20',
                  'SNMP, SMTP, DNS, VPN, VLANs',
                  '10.3.6, IBM DB2, Eclipse, Apache Tomcat, IBM clear case.',
                  'GIT, SVN, Jenkins, IBM WebSphere, Maven, Node-JS.',
                  'JDBC, PL/SQL, SQL, Windows XP, Oracle.',
                  'curriculum and training material.',
                  'and after each sprint and prioritized requirements during each sprint.',
                  'blocks for successful sprints, which led to the transition of releases on time.',
                  'the COTS issues and training on day-to-day basis, during the UAT phase.',
                  'CP3, Edit plus, Rally, SOAP UI, Drools, Putty',
                  'log4J, Subversion SVN, JIRA, SCRUM, My Eclipse/Eclipse',
                  'JS, UNIX, Hibernate, Oracle11g, Agile Methodologies, Selenium.',
                  'ability to prioritize and work independently on multiple ongoing tasks',
                  'Configuration ClearCase, CVS, VSS, SVN, Ant, Maven',
                  'IBM Process Designer, SOAP UI, No SQL, Clover, Crucible',
                  'Tool suite (STS 3.2), Hibernate, Apache Wicket 1.6, Angular 1.4, Oracle 10g',
                  'Ant, Oracle SQL, REST, OpenStack API, Jackson, Splunk, JavaScript, Dojo Toolkit',
                  'ST, JavaScript, JUnit, Unix, Linux, Apache Tiles 2.2, IBM RAD, Rally',
                  'Professional and Dental Claims, Medicare and Medicaid claims, CMS compliance, HIPAA 40',
                  'PorePoint, MS outlook, Messenger',
                  'and performance testing, production and post production support.',
                  'performed both independently and with a team',
                  'efficiently both independently and with a team with confidence',
                  'ons, get commitment to actions from individuals at all levels',
                  'anager for successfully completion of the above mentioned projects.',
                  'SOAP Web Servers Web Logic, Web Sphere, Apache Tomcat',
                  'Cassandra, JIRA, MySQL, Kerberos, Amazon AWS, Shell Scripting, SBT, Git, Maven.',
                  'IX Shell Scripting, SOAP, REST services, Oracle 10g, Maven, Agile Methodology, JIRA.',
                  'CDH distribution, Java, Eclipse, Shell Scripts, Tableau, Windows, Linux.',
                  '(Session Beans), Log4J, WebSphere, JNDI, Oracle, Windows XP, LINUX, ANT, Eclipse.',
                  'Maven, SVN, Git, Jenkins, JIRA, TOAD, SQL Developer.',
                  'HTML, HTML, Java Script, jQuery, Couch DB, Bootstrap, ANT, XML, XSLT, Restful.',
                  '1.5, SOAP, PL/SQL Developer, SAX, DOM, JSF, WSDL, XML and JUnit.',
                  '10.3, SOAP, PL/SQL, DOM, WSDL, JAXP, JAXB, XMLand Eclipse IDE.',
                  'driven methodology, Agile Methodology –SCRUM',
                  'JUNIT, Java8 , Cloud Foundary ,Reactive Systems, Reactive programming',
                  'pring Web MVC, Hibernate 4.0, WSDL, SOAP, SQL, HP Quality Center, JUNIT, Ant.',
                  'DHTML, JavaScript, AJAX, Oracle, SOAP UI Tool, SVN, SQL, ANT.',
                  'javaScript, AJAX, SOAP UI Tool, SVN, SQL, Apache Axis 1.2, ANT, JUnit,.',
                  '(OCA) Sun Certified Java Developer (SCJD)',
                  'SQL, Google Web Kit (GWT), Jira, python, Cassandra, SOA.',
                  '10g, NoSQL, Junit, Maven, JBOSS, SQL developer, Junit, Maven, SVN.',
                  'Log4J, ANT, JUnit, Mockito, MongoDB, Eclipse, Pivotal Cloud Foundry.',
                  'OOAD, Mockito, Groovy/Grails, PL/SQL, AJAX',
                  'engine, JBoss, Axis 2 Web Services, WSDL, XHTML, SOAP, PL/SQL.',
                  'player, excellent interpersonal communication skills.',
                  'Performed Integration Testing and System Testing and bug fixing.',
                  'for the planning of testing schedules and prioritizing the deliverables.',
                  'Used MS Visio for flow-charts, process model of the application.',
                  'Point,Access Methods Waterfall, SCRUM',
                  'that can be analyzed and understood to prepare the to be state of the project.',
                  'from manager. Used HP Quality Center to track defect management life cycle',
                  'defect management life cycle. Provided support for few months after deployment of the project.',
                  'for the client to visualize the end product and suggest required enhancements.',
                  'AJAX, Angular, AJAX, XML, jquery, JSP',
                  'SQL, Tableau, TFS, SharePoint, Service-Now, Java, Unix, API, MangoDB.',
                  'SharePoint, TFS, Postman, Sitecore, Adobe AEM, Tableau',
                  'Point, SOAP UI, Postman, PeopleSoft.',
                  'SQL, OLAP, RACI Matrix, BRD, FRD, SRS, ETL, TFS, Sharepoint.',
                  'Java Persistence API. Education Bachelor of Engineering (Computer Engineering) University of Jammu',
                  'METHODOLOGY- AGILE, WATERFALL',
                  'Server 10.3.4, Oracle, Log4j, JDBC, JDK 1.5, JUNIT 3.8,',
                  'Workshop for WebLogic, Log4j, JDBC, JDK 1.5, JUNIT 3.8, Mockito.',
                  'IDE, SOAP, WebLogic application server, Oracle 10g.',
                  'HTML, XHTML, XML, XSLT, UML, Rational Rose, ANT 1.5.3',
                  'Script. DataBase MS SQL Server, MS Access.',
                  'Server (TFS) 2015,EDI ANSI X12/HIPAA',
                  'MS Word, Excel, Project, Visio, SharePoint, Appian',
                  'Pro, Rational Rose, Oracle 10g and Window XP',
                  '2013, Agile Scrum,Visio,ERP, SQL.',
                  'Pro, Rational Rose, MS-Visio, MS Office, SQL',
                  'MS Word, Excel, Project, Visio, SharePoint, Appian',
                  'Pro, Rational Rose, Oracle 10g and Window XP',
                  'Requisite Pro, Rational Rose, MS-Visio, MS Office, SQL',
                  'Rational Rose, Test Director, Oracle 10g and Window XP',
                  'XML, Mainframes and Rational ClearQuest, ClearCase, Windows XP.',
                  'Designer, MS Visio, JIRA, IBM Mainframe, Toad.',
                  'Pro(QTP)9.2, HP Quality Center/ALM',
                  'specifications to support ERP functional design development.',
                  'external customers on ERP system changes and new procedures.',
                  'UAT Testing. Also, prepared user reference documentation.',
                  'Cause Analysis report using MS Office.',
                  '(QTP)9.2, HP Quality Center/ALM',
                  'specifications to support ERP functional design development.',
                  'external customers on ERP system changes and new procedures.',
                  'UAT Testing. Also, prepared user reference documentation.',
                  'Cause Analysis report using MS Office.',
                  'System: Windows, UNIX, LINUX.',
                  'SQL, JMS, HTML5, JSP, CVS, CSS3, MVC, Maven and SVN.',
                  'ML, HTML5, CSS3, ReactJS, XSL, Oracle, SQL Developer, Log4j.',
                  '10g, Apache Tomcat, ClearCase, Log4J, ANT, JUnit, Eclipse',
                  'Tomcat Server, XML, AJAX, JavaScript, Eclipse, GIT, Oracle, PL-SQL.',
                  'Operating Systems Unix (Solaris); Windows; Linux',
                  'javaScript, JSP, Microsoft SQL Server, WebLogic, Jenkins, Angular JS 2, TFS, SOA',
                  'TML, CSS, JUnit, JMock, Jenkins, findbugs, checkstyle, Sonar, Selenium, curl, perl',
                  '7.1, JavaScript, GWT, HTML, CSS, JUnit, Mockito, perl. NetBeans',
                  'Environment: C++, Oracle 11g/12c, SQL, Solaris October 2011, RHEL 5.5/7.1, Java Swing',
                  'Server, Solaris October 2011, RHEL 5.5/7.1, JUnit, JMock.',
                  'Environment: CISCO 26xx and 36xx series routers, RADMAX Wireless Access Point.',
                  'component of Windows© fixing defects. Environment: C++, Windows',
                  'Web Services), EC2, RDS, DynamoDB, S3, Micro services, Swagger.',
                  'AngularJS, Junit, Jira, Maven, Ant, Jenkins, JBoss.',
                  'Restful Services, JAXP, React, JAXB, QC etc.',
                  'Subversion, JIRA, Maven, Selenium Web Driver, Jenkins, Agile, Eclipse',
                  'Case OS & Environment Windows, UNIX, Linux and Mac',
                  'Sphere Application Server, Mockito, JMS, UNIX, Jenkins, JIRA, log4j and GIT.',
                  'WSDL, JIRA, CSS, Log4J, JUnit, Linux, STS, Oracle, Agile Methodology.',
                  'Apache Tomcat Server, JSF, DOJO, JSON, AXIS, MAVEN, JUNIT, JMS, Log4j.',
                  'Oracle Web logic Server, UCM, SVN, LDAP, Windows XP, UNIX, UML.',
                  'JavaScript, JSP, EJB, Java Scripting, CSS, CVS, WebSphere, oracle, Linux.',
                  'SCJP (Sun Certified Java Programmer)',
                  'Office, Visio, SharePoint, Java/J2EE, Jira, Scrum/Agile',
                  'Microsoft Project Server, Microsoft Office, Visio, SharePoint',
                  'Microsoft Project, Microsoft Office, Visio, SharePoint',
                  'bernate, Spring, Hadoop, Linux, UNIX, Nagios, Git, Jenkins, puppet',
                  'PugJS, JUnit, Mocha, Gulp, Chai, Apache HTTP Server',
                  'UDDI, SOAP UI, Axis2, AWS, REST API, JAX-RS, JERSEY, Windows XP.',
                  'SOAP UI, Jboss, Spring web flow, PL/SQL, Dozer frame work, DB2, JQuery, AJAX',
                  'JPA, SOAP UI, REST web services, spring web flow, Boot strap.',
                  'Case, CVS, SVN and TFS.',
                  'AWS Lambda, Amazon IAM, Elastic Search., ElastiCache, AWS Data Pipeline.',
                  'Windows Azure Cloud, SOAP, WSDL, JAXB, Oracle, Design patterns, Ajax, UDDI, WSDL',
                  'Amazon IAM, Elastic Search, GIT, JIRA , Junit, Unix ,Log4j.',
                  'LINUX, XML, Apache Tomcat, Oracle, Perl Scripts, Shell scripts.',
                  'applications, Apache Zookeeper',
                  'ANT, JUnit, Jboss, ECLIPSE LUNA, GIT, and Oracle, MongoDB.',
                  'AWS, Toad, , SQL Developer, putty, WinSCP, Rallydev.',
                  '(SOAP, Rest full), Soap UI, Solr, Splunk, Jenkins',
                  'Tool, HP Quality Center, Eclipse, MySQL and Jboss Server.',
                  'logic Application Server, Hibernate 3.0, Spring, LOG4J, CVS.',
                  'Virginia International University, Fairfax, VA (GPA: 3.89)',
                  'representing all the Supply Chain Management function and the Logistics.',
                  'Bootstrap-3, JENKINS, IBM Rational Rose.',
                  'React JS, Apache Maven, Apache Camel, MongoDB, SOAP UI, Putty.',
                  'WebLogic Server 8.1, UML, Maven, UNIX, SVN.',
                  'Log4J, ANT, JUnit, IBM RAD, and Apache Tomcat.',
                  'SQL Server 2000, log4j, Subversion, Windows NT',
                  'Mongo DB, JIRA, Maven, JUnit, Tomcat, Eclipse IDE, Windows.',
                  'x/4.x, Oracle 12c, Unix and Linux.',
                  'Websphere, WebLogic 8.1, Eclipse3.x/4.x, Oracle 12c, Windows, , Unix and Linux.',
                  'Eclipse3.x/4.x, WebService (SOAP,WSDL), SPRING, FIX protocol.',
                  'Cassandra, SVN, STS and Tomcat, Ant, Eclipse, RAD, , Unix and Linux.',
                  'logic, JavaScript, JUnit, Oracle 10g, , Unix and Linux.',
                  'HTML, AWS, Tomcat, Java Script, SQL Server, Oracle10g.',
                  'and JBoss, Junit, Unix and Linux. Education: Bachelor’s in Computer Science from Osmania University',
                  'Components, Adobe DAM Digital Education: Bachelor’s in Computer Science - Osmania University, India',
                  'Unix, Struts, Applets, Eclipse3.x/4.x, Oracle 12c, Unix and Linux.',
                  'Git, Tomcat, Eclipse IDE, Windows.',
                  '8.1, Eclipse3.x/4.x, Oracle 12c, Windows, , Unix and Linux.',
                  'Eclipse3.x/4.x, Git, Webservice (SOAP, WSDL), SPRING, FIX protocol.',
                  'SVN, STS and Tomcat, Ant, Eclipse, RAD, Unix and Linux.',
                  'HTML, AWS, Tomcat, Java Script, Git, SQL Server, Oracle10g.',
                  'Tools: Java, JavaScript, HTML, CSS, XML, J2EE, HTML, TCP/IP, REST, SOAP, SOA, Visual Studio 2020',
                  'Engineering from Pune University, India Experience Fiserv Business System Analyst, Alpharetta(GA)',
                  'Excel 2010, Soap UI, Jira, MS Visio, Scrum(Agile)',
                  'ML, MS Office and Windows XP, Rally, SharePoint',
                  'MS VISIO, MS Project, MS Outlook, SharePoint',
                  'SharePoint, MS PowerPoint, MS Excel, OLAP, XSD, SQL, Windows.',
                  'Excel, Scrum, Data Loader, MS Office, MS Visio, MS Project.',
                  'Platforms Windows 2000/98/95/NT4.0, UNIX',
                  'PowerMockito, Mockito, JAX-WS, JIRA, UML, Maven, HTML.',
                  'AJAX,ApacheJMeter, JQuery, JUnit, Maven, Oracle 10g.',
                  'Angular JS, ReactJS, NodeJs, Ext JS, MAX, SEED. Oracle.',
                  'ML, IBM WebSphere 6.1, Rational Clear Case, Log 4j, IBM DB2.',
                  'Maven, JSTL, TOAD, DB2, Web Logic Server, WSDL, JAX-WS, Apache Axis.',
                  'Reporting Services (SSRS) Operating Systems Windows, Mac OS',
                  'PowerPoint, Excel), MS Visio, SharePoint, DOORS, HP ALM/QC',
                  'PowerPoint, Excel), MS Visio, SharePoint, HP ALM/QC, Axure, JIRA',
                  'MS Excel, SQL server, MS Office (Word, PowerPoint, Excel).',
                  'JIRA, Axure, Oracle, MS Visio, HP ALM/QC, SQL Server.',
                  'from New York University.',
                  'report, ILOG, Agile, Scrum, TDD, Web 2.0, Continuous integration, SOA.',
                  'Jira, Subversion (Svn), Jrules, Scrum, Jms, Apachi Tomcat, Linux, Php.',
                  '.Net, ASP, C, C++, Mainframe, COBOL, PL/I, JCL, SQL, SAS VB, ASP.',
                  '2005, JDBC,JNDI,pl/sql srored procedures,Apache Ant, CSS, eclipse and Log4j.',
                  'PL/SQL, MS SQL Server 2005, XML, Log4j, Jsf, CSS, eclipse, Apache, Ant',
                  'Environment: JAVA, Oracle, Quality Center 9.0, MS Access, TABS.',
                  '10.0, Frog logic Squish, Quality Center 9.0, TFS, MTM.',
                  'status to show the progress of the manual testing effort.',
                  'Operating Systems Windows, UNIX and LINUX',
                  'Junit , GIT, Oracle1, TOAD, UNIX, Maven, Jenkins, Agile Scrum, JIRA',
                  'developer, Mocha ,chai, JUnit, JIRA, cloud foundry, UNIX',
                  'HTML,CSS, JavaScript, Jenkins, log4j, SVN, JUnit, J2ME and Windows',
                  'case, PowerMock, JavaScript, and Oracle, Hudson, TOAD, Maven',
                  'Clear case, Apache, log4j, JSTL, PL/SQL, SOAP, WSDL, Unix.',
                  'Project & Visio, Oracle Procurement tool, etc.',
                  '& Virtualization Education & Certifications Bachelor of Engineering',
                  'XMLSpy Test Framework JUnit, SoapUI Version Control SVN, git',
                  '4.3, XML, CSS3, Junit, Jira, SVN, Log4j, TOAD, UNIX, GWT, Ant, Agile, Camel.',
                  'DAO, AWS, TDD, Junit, Jira, SVN, Log4j, Apache POI, UNIX, Ant, Agile, Scrum.',
                  'MongoDB, Linux, Scrum, Tomcat, SOA, Web Services.',
                  'Cassandra, Kafka, JUnit, Oracle, Team Track, Unix, Web services.',
                  'Web Services - WSDL, JAX-WS, JAX-Restful Services, JAXP, JAXB, QC.',
                  'Clear Case, GIT, Serena Dimensions.',
                  '9i, 11g, Maven, SOA, Design patterns, JUNIT, log4J, WSDL, JSON, JNDI',
                  'MongoDB, AWS, Html, JQuery, Angular JS, JavaScript, CSS.',
                  'Methodologies, Jenkins, JDBC, JSON, AJAX, XSLT, XML, JQuery.',
                  'CSS, MySQL, Web Logic 9.2 Windows XP, RAD.',
                  'Spring Framework, Maven 3.2.3, JSP 2.0, JAVA Script, HTML, CSS',
                  '(Formerly known as Sunopsis), SQL Developer, SQL Navigator'
                  ]

garbage_data_2 = [

    'Responsibilities: Involved in various stages of Software Development Life Cycle (SDLC)',
    'Responsibilities: Developed JSP and extensively used tag libraries.',
    'Responsibilities: Responsible for developing use cases, class and sequence',
    'Responsibilities: Designed and developed Servlets and JSP, which presents the end',
    'Responsibilities: Designed the user interfaces using JSP.',
    'Description: Auto Quote Purchase (AQP) application is one of the',
    'Description: One view is a web based application helping Customer Services',
    'Description: Liberty Mutual Insurance is an American diversified global',
    'Description: This project is mainly an Intranet application meant for',
    'Description: PRS or Price reporting system is responsible to publish',
    '▪ Chairing the planning committee for the “2018 Meet me in Savannah Leadership',
    '▪ Managed the Billing Support Team for One Communications – cellular,',
    '▪ Planned, executed and finalized infrastructure IT projects per strict deadlines and within budget.',
    '▪ Managed international relocation, construction and remodel projects. ▪ Provided support wi',
    '▪ Acted as a Green Belt on Six Sigma Projects involving the improvement of pharmacy submissions.',
    'Project: BEST (SAP and ServiceNow Integration) Responsibilities: Working closely',
    'Project: TeamSite (CMS), Salesforce (OneEMS), Hybris, TrackWise and CAPA Responsibilities: Collaborate,',
    'Project: Demand Driven (D2 Supply Chain) Responsibilities: Responsible for the role of Project Mana',
    'Project: Oracle database migration Responsibilities: Participate in project design review meetings',
    'Education: MBA from Vinayaka Mission, Salem, Tamilnadu, India.',
    'Technologies: Java, Rally, ALM,',
    'Role and Responsibilities: Worked at onsite location for Onshore-Offshore model',
    'Role and Responsibilities: Worked as Onsite Design and Execution Lead for 15 members team.',
    'Role and Responsibilities: Study the entire application & process and propose',
    'Role and Responsibilities: Analyzed Business Requirements and System',
    'Role and Responsibilities: • Involves in Management of Test Plan,',
    'Role and Responsibilities: Testing of the project, includes Tes',
    'Role and Responsibilities: Elicited requirements by identifying system users and conducting',
    'Project Description: Initiated Loan Or',
    'Project Description: This project is intended to',
    'Project Description The project aimed to build and',
    'Project Description: To provide a comprehensive electronic',
    'Project Description: This project involved the development of',
    'Project Description: Initiated Loan Or',
    'Project Description: This project is intended to',
    'Description: TCF is a national financial holding company that',
    'Description: T-Mobile US is a United States based wireless network operation',
    'Description: Aetna Inc. is an American managed health care company',
    'Description: Well Mark Blue Cross and Blue Shield is a leading insurance c',
    'Description: This module is for closing account for Lloyds Banking Group. The',
    'Description: Prime Soft Solutions Inc offer Product Development, Enterprises',
    'Digital Loan Process System The project was focused on developing an automated Mortgage',
    'The project was intended to integrate different modules of',
    'Project The project was aimed at creating Enterprise Data Warehouse',
    'The project aimed for a Merchant Payment Solution Gateway from America',
    'Online Banking Facility The project scope was to provide online banking',
    'Banking Web Application micro servicesThe project was focused on develop',
    'Credit Card Application System The project dealt with implementation of',
    'Spearheaded IT system development, enhancement & technology implementation initiatives',
    'Spearheaded a crucial project in partnership with Google AdW',
    'Investments and technology implementation projects and participated in',
    'Spearheaded the administration & operations of the company by providing',
    'New York Institute of Technology New York, NY Promoted as a Graduate Assistant',
    'Assessed business needs; Increased clientele through developing and presen',
    'Equity Capital Markets New York, NY Analyzed company financial, valuation, business',
    'Fifth Third Bank is one of the nation’s largest diversified financial services',
    'Mississippi Division of Medicaid’s Management Information System (MMIS) had to comply with the CMS',
    'I worked as a business analyst in the project intended to make the existing application',
    'Prime therapeutics is leading Pharmacy benefit Management Company. I worked with member',
    'The project was aimed at successfully implementing a system that provides an integrated,',
    'Description: Liberty Mutual is an insurance company which offers auto & vehicle insuran',
    'Description: Express Scripts provides integrated pharmacy benefit management services i',
    'Description: The project was to enhance the existing online applic',
    'Description: E-Trade Financial Corporation is a U.S based financial services company head',
    'Description: US Cellular is the fifth-largest full-service wireless carrier in the United States, ',
    'Description: This is an internet based online shopping website in which products will be sold',
    'Description: The product was online banking system which provides personal savings details to',
    'WSDL, PL/SQL, Oracle 12C, STS, SVN, Maven, IntelliJ IDE, Jenkins, Selenium, JIRA,',
    'Guidewire Software Focus on many services the section of job role was funds or credits ',
    'Online billing system (OBS) project provides enhancements to existing online billing application. OBS is',
    'MASTERS IN MOBILE AND UBQUTIOUS COMPUTING, TRINITY COLLEGE DIUBLIN, DUBLIN-02,',
    'BACHELORS OF ENGINERRING IN ELECTRONICS AND COMPUTERS, KONERU LAKSHMAIAH UNIVERISTY',
    'Designation: Project Manager Project Description: Account Management Planner (AMP)',
    'The Account Management Planner Profiler Tool is a web-based tool to provide the client’s Account',
    'This project is a development project and support project which involves',
    'This project is a Service support project and involves day to day',
    'Client Description: Atmel Corporation is a worldwide leader in the',
    'This project is a development project which aims at redesign of Atmel.com’s',
    'Client Description: MOHH (previously known as Health Corporation of Singapore)',
    'This project is the Corporate Website revamp project for MOHH which is MOHH’s main',
    'Client Description: The Singapore Sports Hub is a Public-Private-Partnership (PPP)',
    'The project involves support and maintenance of BIS application for Singapore Sports',
    'SKM has set up SAP Business Objects (BO XI3.1) environment called “INSIGHT”',
    'Client Description: Citi, the leading global bank, has approximately 200 million',
    'Citi has a huge Financial business which needs to be monitored and analyzed daily.',
    'Client Description: D&B is the world’s leading source of',
    'D&B has implemented Oracle E-Business Suites - Account Receivables, Account Payables and',
    'D&B planned to remove the grace period access to the contracts which were not renewed and',
    'This is the maintenance and support project where in day to day analysis and decision',
    'This project was initiated to create a reporting structure in UK and Ireland from scratch.',
    'This application was an enhancement on Phase I and it included creation of many reports',
    'This application is used to implement the Global reporting for HRCC in Business Objects',
    'The purpose of this application was to migrate from BO 5.X to BO XI R2.',
    'Technology: Business Objects, Sybase IQ, Oracal',
    'The purpose of this project was to create the top 10 DnB’s customers and do the',
    'The purpose of this project was to enhance the features of the preferred Pricing',
    'The purpose of this project was to design and automate the back-office of the Preferred',
    'Responsibilities: JSF Portal Framework atDecember 2021ation Tier and Faces Servlet acts as',
    'for Marketing and Sales Operations Projects Initiate and plan implementation of projects.',
    'for Dealer Claims, Warranty Systems and Overall Dealer Website Projects',
    'for multiple Vehicle Supply Chain Applications Lead the enhancements and',
    # 'Developer/Module Lead/Business Analyst for Reporting and Liquidity Management Systems Projects Design and',
    'Description: This project which has been',
    'Description: Target Enterprise, Inc Senior Engineer will be responsible for writing, developing',
    'Description: This project, which has been developed to significantly reduce the development',
    'Description: DIPR (Disney ID for Parks and Resorts): The main purpose of the application is to capture',
    'Description: Austin County State Bank is a leading property and casualty insurance company based in Texas.',
    'Description: GE Healthcare provides transformation medical technologies and services helping to deliver patient',
    'Program Manager handling projects for Metlife',
    'Responsibilities: Mentored the core IT team to follow Agile practices instead',
    'Business Solutions Group Proactive proposals, Response to RFP’s and collateral preparation',
    'Locations: Navi Mumbai, India Identified problem areas and suggested ways to tackle business',
    'Locations: Bangalore, India Led a team for a prestigious firmware verification project',
    'Project was involved working on the design and configuration changes as per the PPACA',
    'Duties included building various applications for physicians, clearinghouses,',
    "Description: : eClaims is the New York State Workers' Compensation Board's implementation",
    'Description: Broadcom Limited is a diversified global semiconductor leader built on 50 year',
    'Description: NJM was originally implemented in a mainframe and this project is',
    'Description: Uhlig LLC and its subsidiaries have played a leading role in the growth',
    'Description: ICICI Bank is an Indian multinational banking and financial services company.',
    'Description: Infotech Enterprises focus includes aerospace, defense, rail',
    'Project: Mortgage Loan Origination System Project Description: Scope of the',
    'Project: Data Warehouse Project Description: The project specifically aimed to',
    'Project: Wealth Management System Project Description: The idea was to develop a',
    'Project Description: Scope of the project was to develop a intranet application to',
    'The scope of the project was to create a single, integrated system that will execute',
    'Project: Foundation Services (GLS, ConfigServices) Technologies: Mulesoft, Cloudhub, Java,',
    'Project: NetSim (Network Simulator) – lON Technologies: Hadoop (MAPR), HDFS, Spark, YARN,',
    'Project: Cellular Network Administrator (CNA) , Technologies: Java, C++, JavaScript, XML, XSLT,',
    'Project: MST (MUI System Testing), Offshore Technologies: Java, JavaScript, HTML, CSS, JDK, JDBC,',
    'The Project was about multi-level report management specially that of an MBR',
    'The project was about redesigning and implementing the recent requirements',
    'The project was for up-gradation and further enhancement of the mortgage',
    'The project was to enhance AML protocol and update December 2021 software',
    'The project was trading of commodities using Market Data System (MDS)',
    'The project at Saxo Bank was about their financial services to the clients.',
    'Magellan offers full service PBM (pharmacy benefit manager) that combines',
    'SCI Group is the vendor of U.S Department of Interior who is',
    'The project was a web based application involving the automation of',
    "Omgeo Connect, a complementary offering to Omgeo's existing solutions, Omgeo Connect provides",
    'Netsoft Microsystems specializes in Offshore and Inshore Development of Software',
    'Responsibilities: IT Business Analyst & QA Analyst for Commercial Banking, deal',
    'Responsibilities: Facilitated Scrum planning meetings to coordinate between Clients, Product Owner',
    'Responsibilities: Documented and validated the business process flow of Online Securities',
    'Responsibilities: System study and preparing business requirement documents',
    'Responsibilities: IT Business Analyst for Mortgage Banking, dealing with releases',
    'The scope of the project was to develop an application that can help customers',
    'The Intent of the project was to develop a Fixed Income Operations tool',
    'The Scope of this project was to improve the overall efficiency in document',
    "As a part of the cornerstone 2015 Audit Map and in pursuit of AEXP's Enterprise Data Production",
    'Enterprise Marketing CRM data model to ensure the campaign data mart designs are optimized for',
    'Primary Responsibilities: Plan, execute, and finalize projects within triple constraints ',
    'Primary Responsibilities: Manage multiple projects under single program umbrella specifically',
    'Primary Responsibilities: Basic functional design. Created UML diagrams using Microsoft Visio',
    'Primary Responsibilities: Reviewing and assessing existing data processing methods and ',
    'Primary Responsibilities: Involved in Analysis and design of the required projects.',
    'Primary Responsibilities: Involved in Analysis and design of the required projects.',
    'Primary Responsibilities: Involved in Analysis and design of the required projects.',
    ' Education Education Master of Computer Application',
    'Responsibilities: Developed the Generic Java concurrency Framework to execute',
    'Responsibilities: Involved in the Project Setup process of routing and filter',
    'Responsibilities: Involved in Full Cycle of Software Development from',
    'Responsibilities: Implemented the application using the concrete principles laid',
    'Responsibilities: Involved in Full Cycle of Software Development from Analysis through',
    'Responsibilities: Involved in Full Cycle of Software Development from Analysis',
    'Responsibilities: Involved in Full Cycle of Software Development from Analysis',
    'Responsibilities: Developed Spark applications using Scala utilizing Data frames and Spark',
    'Responsibilities: Part of Big Data Center of Excellence (CoE), responsible for designing',
    'Responsibilities: Developed multithreaded Java based Input adaptors for ingesting click',
    'Responsibilities: Migrated the needed data from MySQL into HDFS using Sqoop and importing',
    'Responsibilities: Implemented a Web based Application using Servlets, JSP, spring, JDBC, XML.',
    'Responsibilities: Involved in Requirements analysis, design, and development and testing.',
    'Golan Technologies range from turnkey solutions to custom, client-driven solutions in a',
    '(December 2021) Primarily involved in: Program & Project management for Oracle ERP,',
    'Optimize the Enterprise IT Support, Change & Release Management across various technologies',
    'Transition, Transformation & Reengineering of Processes for setting up Shared Service',
    'Team Building, Plugging Lacunae in the December 2021 Processes by having proper Documentation',
    'Prepare project plans, requirements, constraints and assumptions, Stakeholder Analysis',
    'Worked as a Solution Architect for implementation of Multiple Reporting Currency (MRC)',
    'Worked as an Assistant for 4 years in V.K.B & CO. Audit finalization and taxation of',
    'The project I was involved in built data driven strategies to support business operations.',
    'As a part of this project, provided support Investment Management Group and their',
    'As part of this project, worked directly with the Project Manager, Configuration Specialist,',
    'Analyzed clients systems and business processes Successfully implement',
    'Analyzed operating results, explanation of variances, trend and year',
    'Project Description: The project was on create claim management system which has',
    'Project Description: To create a web based application which would automate the claims',
    'Project Description: To develop Customer Billing and Management System',
    'Walgreens provides medical imaging and information technologies,',
    'The company provides a variety of personal insurance products,',
    'ROC Application Automation Facilitate elicitation with the operation',
    'Peertasker Application Conducted walkthroughs and acted as a bridge',
    'Real-time Voice, Video communication with PCPs and Specialists',
    'Requirements Elicitation with the stakeholder. Develop and documented Feature document,',
    'Defined Project plan, scope and cost benefit analysis for the application.',
    'Responsible for Project Initiation, Scope definition, execution, monitoring and close',
    'Managed lot of web applications in both PHP and ASP.NET.',
    'Responsible in managing project s in Waterfall. Involved in',
    'Responsible for placing the project proposals, preparing proposals and chat',
    'BSc in Spl.Physics, The American College (Madurai, India), July 2000 - May 2003 DNIIT program in NIIT,',
    'DNIIT program in NIIT , NIIT (Madurai, India), 2003',
    'Description: Staples mainly focuses on developing a Retail application for the',
    'Description: Global Wires Application is a wires payment application',
    'Description: Small Group Renewal(SGR) is the automated solution',
    'Description: CSR Dashboard is an internal facing application, accessed via the Prime View',
    'Description: Sales Tool Project is an Online Auto Insurance Sales tool used to',
    'Description: BSHM builds a system, which helps both Doctor and patient in',
    'Responsibilities: Involved in Requirements gathering, Analysis,',
    'Established release processes for change management and published release status',
    'Environment: Windows 8.0/8.1, SQL Server 2012, Visual Studio 2013, Microsoft Test Manager,',
    "Helped develop a single dashboard for Service Agent's desktops of all agent-required",
    'Technologies & Tools: Quality Center, Database technologies (SOAP UI, Endeca, and Facade)',
    'Charged with analyzing the applications under scope for release, defined team work',
    'Prioritized apps for general availability releases, provided project monitoring/tracking,',
    'Environment: Windows 2003, XP, WTT, C#, Maui framework, AAF framework, XML',
    'Environment: Vista Client, Vista Server, Windows Server 2003, XP, WTT, JScript',
    'Description: StubHub.com is a product company building a ticketing platform which',
    'Description: Worked as a Sr. Developer with Citibank on the CitiTrade Application at Citicorp.',
    'Project Description: In an effort to be HIPAA compliant, Anthem was changing',
    'Description: SBC is one of the largest telecommunications companies in the world.',
    'Brief Description: For a large US-based financial institution: Wipro migrated 300+ applications',
    'Projects Implemented: 1)Citrix on AWS -1 year Project 2)1500 Server Patching-wana',
    'The Miller Brewing Company is an American beer brewing company owned by SABMiller.',
    'RSA Insurance Group plc (trading as RSA, formerly Royal and Sun Alliance)',
    'The Miller Brewing Company is an American beer brewing company owned by SABMiller.',
    'CDPHP is a leading health insurance company which was award JD power award for 2017 year',
    'The Miller Brewing Company is an American beer brewing company owned by SABMiller.',
    'RSA Insurance Group plc (trading as RSA, formerly Royal and Sun Alliance) (LSE: RSA) is a multinational insurance',
    'The Miller Brewing Company is an American beer brewing company owned by SABMiller.',
    'Description: StubHub.com is a product company building a ticketing platform which',
    'Description: Worked as a Sr. Developer with Citibank on the CitiTrade Application at Citicorp.',
    'Project Description: In an effort to be HIPAA compliant, Anthem was changing the',
    'Description: SBC is one of the largest telecommunications companies in the world.',
    'Brief Description: For a large US-based financial institution: Wipro migrated 300+ applications',
    'Description: This project was to implement IV&V on MMIS and Medicaid Eligibility System.',
    'Description: The project involved TriZetto heading the IT solution for Blue plans.',
    'Description: This project mainly deals with the large web based applications used in',
    'Description: This project involved the implementation of a web based portal for billing',
    'Project: Medical Description: Optum is powering modern health care to create',
    'Project: Flooring industry Description: Mohawk Industries (NYSE: MHK) is an American flooring',
    'Description: The Aviva Life Insurance India is widely recognized Insurance provider.',
    'for a high priority $350 MM IT Business Strategy Transformation Program.',
    'for Global Business Analytics Project. Planning & designing Process Reengineering',
    'EMC 3.0 Project: The project involves the development and testing of eCommerce',
    'Project: This project involved the development of a Mobile',
    'Project: This project involved the development of a Web based',
    'Project: Develop a web application which enables',
    'Description: Coordinating and implementing Hardware and server request from the initial request,',
    'Description: There are around 70 Operating Companies (OpCo) where each OpCo is comprised of three',
    'Description:The scope of this TITAN project is to update all the applications to',
    'Description: Worked for a merger project where two banks mergedalong with their Global Shares.',
    'Description: This project is mainly about setting the Credit Limits for the',
    'Description: State Street Corporation serves as the custodian for',
    'Description: Bailey Trading Company, LLC in Los Angeles, CA is a USA American',
    'Description: This project is a comprehensive and integrated Hospital Management',
    'Description: NCC Finance provides customized financial solutions to business,',
    'Description : Life, RPS and Annuity Insurance',
    'Project Name : Sigma BI for AIG Technology : IOS and Android Apps, Business analytics-IBM Data',
    'Project Name : In-Flight Exchange (IFX4) ™ Product suite = Supply chain management for Air line services',
    'Project Name : Prepaid and Post-Paid Billing Systems for British Telecom',
    'Roles and Responsibilities Accomplishment: Process Improvement Prepared requirement clarification tracker to',
    'Project Name : Credit Quality Decision Suite this is an integrated solution for',
    'Project Name : PBM – Pharmaceutical Benefit Management system for Merck fortune pharmaceutical',
    'Project Name : HMS version 1.0 Hospital Management system',
    'Description: Pennsylvania is implementing an Electronic Death Registration',
    "Description: Comcast is the second largest pay-tv company in the United",
    'Description: Union Bank is a full-service bank with offices across the United States.',
    'Description: .Healthfirst is a provider-sponsored health insurance company',
    'Description: Highmark Inc. and its health insurance subsidiaries and',
    'Description: Implemented a centralized database that collects and organizes',
    'Responsibilities: Designed and developed user interface using front-end',
    'Responsibilities: Worked in various phases of Software Development',
    'Responsibilities: Gathered requirements from the Product owner and',
    'Responsibilities: Involved in System Analysis and Design methodology as',
    'Description: It allows time tracking for customer and project related tasks.',
    'Project: HPlan Application Description: This project is a comprehensive and integrated',
    'The Vanguard Group is an American investment management company based in Malvern,',
    'The project involved enhancement of POS System to meet new business requirements.',
    'Ebay is a leading US based E-commerce Company providing consumer to',
    'Apollo Hospitals Group, today, is an integrated healthcare organization',
    'Techno soft provides a range of professional services delivered by experts',
    'ROLES AND RESPONSIBILITES: Developed presentation layer and GUI Framework using',
    'ROLES AND RESPONSIBILITES Developed CSS3 style sheets to give gradient effects.',
    'ROLES AND RESPONSIBILITES: Developed User Interface and implementing business process using JSP',
    'ROLES AND RESPONSIBILITES: Performed the requirement analysis by interacting with',
    'ROLES AND RESPONSIBILITES: Worked with System Analyst and the project team to understand the requirements.',
    'RESPONSIBILITIES: Leading offshore teams, and discussing the projet',
    'RESPONSIBILITIES: Followed agile methodology and involved in daily SCRUM meetings,',
    'RESPONSIBILITIES: Good Experience in Agile methodology with',
    'RESPONSIBILITIES: Used spring’s test framework to create integration tests for various',
    'RESPONSIBILITIES: Designed and developed web site application using AJAX, AngularJS,',
    'Responsibilities: Involved in analysis, design, and developing middleware using Servlets,',
    'Responsibilities: Involved in Daily Scrum (Agile) meetings, Sprint planning and estimation of',
    'Responsibilities: Involved in the loan detailsDecember 2021ation module.',
    'Responsibilities: Participated in system design, planning, estimation and implementation.',
    'Responsibilities: Designed and implemented Java Classes, Interfaces, Model design, and interface',
    'Title: Fidelity Portfolio Advisory Services at work (F-PASW) Description: Fidelity Portfolio',
    'Title: iPhone activations for a top retailer in USA Description: This project allows',
    'Project: HAS (health savings account) Description: A Health Savings account (HSA) is a special',
    'Project: TTSL-telecom Description: A web service project for TTSL portal used by the',
    'Project: Resource Management System Description: CISCO is the worldwide leader in',
    'Description: Customer Self Service application is used by all John Deere Financial',
    'Project: System Management Description: The objective of this project is to implement',
    'Description: The goal of this project was to develop an online system to facilitate',
    'Project Description: This project is FedEx Air Operations project.',
    'The main aim of the project is to provide the services',
    'Project Description: This is one of the major projects of JP Morgan Chase Investment',
    'Project Description: This is a credit card statement redesign project.',
    'Project Description: Discover Financial Services is an American financial services company,',
    'Project Description: E-CAS reporting application is new end to end reporting',
    'Project Description: American Express Company is an American multinational financial',
    'Project Description: MakeMyTrip Limited is an Indian online travel agency that holds',
    'Project Description: MyClassBoard is an ERP tool which helps institutions automates the',
    'Responsibilities Actively involved in Analysis, Design, Development, System Testing',
    'Responsibilities: Responsible for gathering and understanding the system requirements',
    'Responsibilities: Design and implemented the backend layer using Hibernate.',
    'Responsibilities: Responsible for analysis, design, development and integration of UI',
    'Responsibilities: Preparing the documentation for High Level design, Low Level design',
    'Responsibilities : Developed data formatted web applications and deploy the script',
    'Responsibilities: Developed presentation layer includes UI components, validations',
    'Responsibilities: Developed presentation layer includes UI components, v9alidations',
    'Responsibilities: Designed and development applications using Rational Unified',
    'worked with a team of Software Engineers in the design',
    'Designed and developed system software and GUI web page',
    'Designed, developed and implemented software for satellite',
    'involved in multiple retail projects and managing various stakeholders,',
    'involved in multiple retail projects and managing various stakeholders,',
    'Manage multiple IT projects in waterfall and Agile environmt',
    'provide solution and streamline B2B and B2C (Buy/Sell & Drop shipment) mod',
    'various retail operations and manage various projects like managing level-II POS',
    'Manage and execute POS system upgrade with order management and inventory management module.',
    'Prepare and Manage project plan for EZ Franchise system and integrate with store',
    'SMPPO (Stock Model Production Planning & Operations) & Inventory Distribution of Cement and clinker.',
    'RESPONSIBILITIES: Worked and developed functionalities in Agile Methodology',
    'RESPONSIBILITIES: Worked on JavaScript to validate input, manipulated HTML elements',
    'RESPONSIBILITIES: Participated in all phases of the project like design,',
    'RESPONSIBILITIES: Involved in the software development life cycle requirement',
    'Certification: Certified Data Scientist i.e. Data Science Professional (from John Hopkins University).',
    'Primary objective of the project was to address',
    'The prime objective of the IoT (Internet',
    'The purpose of the Client Integration initiative',
    'Prime focus of the EXIM/Universal Cart',
    'The prime objective of the project is to replace',
    'The purpose of the project was to report the back-end Teradata IW',
    'The primary goal of the project is to extract common services such as',
    'The project was to report corporate data warehouse for online trading',
    'Responsibilities: Designed application using UML. Elaborated on the Use Cases',
    'Responsibilities: Gathered system requirements for the application and',
    'Responsibilities Designed use case diagrams, class diagrams, and sequence',
    'Responsibilities: Implemented the advance validation rule model for administering',
    'Responsibilities: Involved in all the phases of SDLC including Design & Analysis',
    'Responsibilities: Developed custom JSP tags for Pricing, Order Management,',
    'The GSI [eBay] Remote Tools system is a browser-based application designed to allow users',
    'Project Description: American Express is a leading credit card company with a vast',
    'Project Description: World Fuel Service is a leading global fuel logistics company',
    'Description: Gravitant is a company that develops cloud-based software to',
    'Responsibilities: Involved in the review and analysis of the Functional',
    'PROJECT DESCRIPTION: The project was aimed at creating a Data Mart to provide Educational',
    'PROJECT DESCRIPTION: The scope of the project was to offer a unified shopping experience',
    'PROJECT DESCRIPTION: Business Technology Operations team wanted an application that can',
    'PROJECT DESCRIPTION: Project1: The primary objective of this project is to capture day to day',
    'PROJECT DESCRIPTION: Worked for Commercial Intelligence Teams, one of the key departments of',
    'PROJECT DESCRIPTION: The scope of the project was to add a shipping cost estimator which calculates',

    # 'Title: Inter Company Payment Process Anixter is a leading global supplier of',
    # 'Title: DC2 Phase 0 ICP– Life Quote The Integrated Customer Platform',
    # 'Title: Cost Basis Day3 Project: USAA is a fully integrated financial services company',
    # 'Title: Enterprise Wires Services Project: USAA is a fully integrated',
    # 'Title: Real-Time IDV OFAC Project: USAA is a fully integrated financial services company',
    # 'Title: GIM2 Report Changes Project: USAA is a fully integrated financial services company',
    # 'Title: CLO DB Purge/Archive Project: USAA is a fully integrated financial services',
    # 'Title: USAA-Bank CLO E-Fulfilment-OF Project: USAA is a fully integrated financial',

    'The aim of the project to develop an Enterprise Level Information Intelligence',
    'The project was to completely migrate the Legacy CRM Application on the Premises',
    'The aim of project was to build an upgraded Data Warehouse System for',
    'The client is one of the leading providers of P&C and life insurance service.',
    'The project was about creating an application',
    'The project was to develop Highly Responsive Pages (HRP) and',
    'The project was to enhance the Business Process of an E-commerce website',
    'The scope of the Project is to enhance the application for Ecommerce Platform',
    'Project Description The Materials Management/eTag program provides platform of as',
    'Project Description SMF enables GE Energy Sourcing Vendor Management team to',
    'Project Description AP Conversion is an application to migrate all',

    'The aim of the project to develop an Enterprise Level Information Intelligence',
    'The project was to completely migrate the Legacy CRM Application on the Premises',
    'The aim of project was to build an upgraded Data Warehouse System for Workforce Development',
    'The client is one of the leading providers of P&C and life insurance service.',
    'The project was to develop Highly Responsive Pages (HRP) and mount them on the Enterprise',
    'Responsibilities: Workedon Requirement analysis, gathered all possible business requirements',
    'Responsibilities: Plan and execute phases of the software development lifecycle utilizing',
    'Responsibilities: Involved in extensive meetings with business and design teams in',
    'Responsibilities: Wrote front end code for developing new pages using HTML, CSS, JavaScript.',
    'Project: Risk management application used to assess market risk involved in',
    'Project: Coast Healthcare developed a software suite to equip partners and providers with',
    'Project: CSP project was an effort to integrate and consolidate the Medicaid',
    'Project: Arch Insurance offers a wide range of healthcare, property, casualty and',
    'Project: Electronic Medical Record Management System: A Physician Support System was built to',
    'Project: NC Mutual Life Insurance was developed a new Java based web app to manage online',
    'Responsibilities: Analyzed Information system and December 2021 working model to optimize',
    'Description: It is a national sales executive responsible for national sales of Credit',
    'Description: LILEE Systems, an Inc. 500 award-winner, is capitalizing on the growing',
    'Description: UHC is a Preferred Provider Organization (PPO) that has created a network',
    'Description - This Project is about to implement the Automated Adjudication System,',
    'Description: The project objective is building fully insured on Facets to deliver a',
    "Description: DuPont is one of the world's most innovative companies.",
    'Description: Well Mark Blue Cross and Blue Shield is a leading insurance',
    'Description: The move money services help general users and investment users,',
    'Description: Sprint Synaptic Storage as a Service is a web services-based',
    'Description: State Farm is a group of insurance and financial services companies',
    'Description: Techno soft provides a range of professional services delivered by',
    'Description: Udyog software is a pioneer in providing tax automation products,',

    'LINUX, Ubuntu Design Tools Rational Rose, Microsoft Visio, UML, LDAP Protocols SOAP',
    'This is a SOA layer developed for AMEX Bank using RESTful web services, spring and Hibernate.',
    'DNR is dedicated to working with the citizens and businesses of Wisconsin',
    'is a part of the Cyient group, leverages its business process',
    'The Online Constructor enables construction project personal to seek and identify pre-qualified',
    'Project involved Surveillance designed to detect violations of exchange trading rules,',
    'An internal ovation project designed to service the customers in maintaining the accounts,',
    'An architectural and security model gateway project, integrated portal administration',
    'Authorization at POS to accept open to buy modifications for returns, payments, sales',
    'Description: The Capital One Loan Origination System (CLO) deals with the',
    'Description: Order management project provides the functionality to test',
    'Description: This project is a comprehensive and integrated Hospital Management System',
    'Description : The project involved an application and performance Management Software for',
    'Description: The project involved an application and System Management Software',
    "CLI: DB2 Call Level Interface (DB2CLI) is IBM's callable SQL interface to the DB2 family of database servers.",
    'Description: The project involved LIs FVT for DB2 server version 9, 9.5 and 9.7 Faster Redistribute',
    'Description: The project involved Informix 7x, 9x and 10x Online Server QA.',
    'Description: Online 5x server was first Informix server with client-server architecture',
    'Project Summary: The project focus on making it easier to collect energy consumption',
    'Project Summary: Employer Sponsored alerts Micro services designed to expose the data',
    'Project Summary: TRAMS (Thomson Reuters Application Management Suite) is a standardized',
    'Project Summary: OptumRx is a project in to manage customer prescription medications',
    'Project Summary: The project involves creation of a single, integrated system which',
    'The Accredited Standards Committee X12 (ANSI X12) standard EDI transactions and integration',
    'PROJECTS: EviCore Platform – (PAC) Post-Acute Care EviCore Platform – (RAD CARD) Radiology Card',
    'PROJECTS: CA (California)- MedImpact Pharmacy to Tapestry 837- 835 TDS integration Health',
    'PROJECTS: Federal Employee Program Stream Line Project (FEP Streamline) Data Warehouse',
    'PROJECTS: EHR Data Migration- Data Warehouse Implementation Facets/Business',
    'ROJECT: Cigna Healthcare claims BPM HIPAA 4010-5010- Migration BAM Dashboard SOFTWARE & ENVIRONMENTS:',
    'PROJECTS: Administrative simplification SOFTWARE & ENVIRONMENTS: MS Office, Project, Visio,',
    'PROJECTS: TTK BI Reporting TTK CPD Business Process SOFTWARE & ENVIRONMENTS: Rational',
    'Project Description: TMNA offers the security of nearby',
    'Project Description: Davita is one of the largest kidney',
    'Project Description: National Association of State Boards of',
    'Project Description: Copart makes it easy for Members to find, bid and win',
    'Project Description: Aricent is a global design and engineering',
    'Roles &Responsibilities: Involved in all phases of project from analysis and requirement',
    'Roles &Responsibilities: Involved in Software Development Life Cycle (SDLC) starting from',
    'Roles &Responsibilities: Analysis of the Business Requirement and Functional Specification',
    'Roles &Responsibilities: Involved in System Requirement study, conceptual design and designing',
    'Responsibilities Involved in analysis, design and documentation of the application',
    'Responsibilities Development, enhancement and testing of the Web Methods flow services',
    'Responsibilities Responsible for coding with the use of Object Oriented Principles',
    'Responsibilities Design, Development, testing and debugging of new software and',
    'Responsibilities Involved in various phases of Software Development Life Cycle (SDLC)',
    'Project Description: Customer Accounts API Development/Enhancement and Migration to',
    'Project Description: Customer Accounts Interface retrieves all financial accounts',
    'Project Description: The Auto Insurance Application allows customers to access',
    'Description: Maintain manual details of blood camps, donor details, blood stock',
    'Project Description: Cipla Health Insurance Hub with single-point Internet access',
    'Project Description: This project involves developing a J2EE based framework to',
    'PayPal Holdings, Inc. is an American company operating a worldwide online payments',
    'Liberty Mutual Insurance, is an American diversified global insurer, and the fourth-largest',
    'Capital One is a financial holding company which offers a broad spectrum',
    'HealthNow is an American managed health care company, which sells traditional and consumer',
    'Project Description: JerseySTEM is a network of parents, professionals,',
    'Project Description:Worked for Banking Product Services',
    'Banking and Financial Services',
    'Worked with UBS to provide an enhancement of an existing',
    'Worked with Eclerx under the prestigious flagship project of',
    'An Enterprise level project that involved an upgrade of the Oracle',
    'Insurance application enhancement for employees to manage retirement',
    'The investment banking project for Kotak Mahindra was to add more features',
    'The project scope was to develop a web application for an education company ‘Jamboree’.',

    'Certifications Six Sigma Green Belt Sun Certified Java Programmer Professional Scrum Master',

    'Project Description: As a Java Developer, I was involved in developing a web',
    'Project Description: Our Project is a fully integrated web-based project whose',
    'Project Description: Our Project is a migration project which was implemented',
    'Project Description: Our Organization has internal projects resource sharing and',
    'Description: Project is a web-based application where a customer can get necessary',
    'Project Description: SAP provides a wide range of health services including full',
    'I was contracted as a Business Analyst designed to cater to the needs of the',

    'Responsibilities: Facilitate meetings with stakeholders and Subject Matter Experts(SME)',
    'Responsibilities: Participated in System Design Discussion. Worked in an Agile environment,',
    'Responsibilities: Prepared system documentation for upgrades and new system functionality.',
    'Project Description: Marketplace Web Portal Molina Healthcare is one of the Obama Healthcare',
    'Project Description:The project included the HIPAA Business Analysis activities, primarily for',
    'Project Description: Assurant health care is one of the largest health care organization',
    'Project Description: The project Claims-Funds involved creating a new UI for better',
    'Project Description: Humana is one of the leading Health Insurance companies in the US.',
    'Office of Attorney General Child Support Division provides a full range of products and services',
    'Atlas air is one of the largest American-based company in insurance business.',
    'This Migration project was initiated to enhance the Pershing’s ability to meet the regulatory',
    'This project was conducted to develop a software to track all the information from the applicants',

    'It is an investment company the purpose of the project is to analyze the data',
    'Program Details: GE HEALTHCARE is a member of the General Electric group of companies.',
    'Environment Oracle SOA 12C and webMethods Project Description WebMethdos Remediation',
    'Environment webMethods 8.x, (Integration Server, Broker, JDBC Adapter, Trading Network)Project Description',
    'Environment webMethods 8.x, (Integration Server, Broker, JDBC Adapter, Trading Network) Project Description',
    'Environment webMethods 7.x, (Integration Server, Broker ,JDBC Adapter) Project Description This interface is',
    'Environment webMethods 8.x, (Integration Server, Broker , BPMS, JDBC Adapter) a) Project Description Designed',
    'Team Size Project :10 Module : 2 Environment Software Tools: WebMethods Integration Server Platform,',

    'Team Size Project :2 Module : 1 Environment Software Languages : webMethods flow language, Java Database :Oracle 12i',
    'Team Size Project :1 Module : 1 Environment Software Languages : WebMethods flow language, Java Database :Oracle 12i',
    'Team Size Project :2 Module : 1 Environment Software Languages : WebMethods flow language, Java Database :Oracle 11g',
    'Team Size Project :1 Module : 1 Environment Software Languages : WebMethods flow language, Java Database :Oracle 12i',
    'Team Size Project :3 Module : 1 Environment Software EAI: webMethods 6.1 & 6.5 platform (Integration server.',
    'Team Size Project :1 Module : 1 Environment Software Languages : WebMethods flow language, Java Database :Oracle 9i',
    'Team Size Project :1 Module : 1 Environment Software Languages : WebMethods flow language, Java Database :Oracle 9i',
    'Team Size Project :8 Module : 1 Environment Software Languages : Tibco flow language, Java Database :Oracle 9i EAI',
    'Responsibilities: Prepared Business Requirements Documents (BRD)',
    'Responsibilities: Wrote BRD and Use case documentation for requirements analyse',
    'Responsibilities: Providing business solutions developed from a unique',
    'Responsibilities: Validated and processed insurance policy change request',
    'Responsibilities: Played a vital role in the creation and maintenance of',
    'Responsibilities: Prepared Business Requirements Documents (BRD), System',
    'Responsibilities: Wrote BRD and Use case documentation for requirements analysis',
    'Responsibilities: Providing business solutions developed from a unique',
    'Responsibilities: Validated and processed insurance policy change request',
    'Responsibilities: Played a vital role in the creation and maintenance of',
    'Project involved in developing a Web-based Inventory. The application allows the clint to',
    'This is an online insurance quoting and policy application, which enables customers to',
    'This Banking application allows customers to access their Bank account through World Wide',
    'Web-based Java application which provides rates for the insurance policy premiums based',
    'Responsibilities: Involved in the Design, Development Phases of quotes, customer service modules.',

    "BP is one of the world's leading integrated oil and gas companies. Like all",
    'Development of financial reporting for one of the largest hospitals, based on clinical variables',
    'IP Device Management and Reporting solution is used to manage fault and performance for SNMP',
    'Port legacy NetExpert C++ application to run on RHEL5.5 platform. Internationalization (i18n)',
    'This is an enterprise server side solution which added service assurancen',
    'Responsibilities: Provide software and network consultation providing networking consultancy and',
    'Virtual DOS Machine in OS is used to support legacy 16 bit application.',
    'Responsibilities: Design and develop and maintenance of Cisco IOS device driver software.',
    'Locomotive Distribution and Assignment system has been primarily supported for strategic planning',
    'Islamic banking is banking or banking activity that is consistent with the principles of',
    'Intellect is a banking product which is an integration of Core System, Lending System, Collateral',
    'Learning Management System is the E-learning based product .The main aim of this product is learning',
    'Responsibilities: Prepared Business Requirements Documents (BRD)',
    'Responsibilities: Wrote BRD and Use case documentation for requirements analyse',
    'Responsibilities: Providing business solutions developed from a unique',
    'Responsibilities: Validated and processed insurance policy change request',
    'Responsibilities: Played a vital role in the creation and maintenance of',
    'Responsibilities: Prepared Business Requirements Documents (BRD), System',
    'Responsibilities: Wrote BRD and Use case documentation for requirements analysis',
    'Responsibilities: Providing business solutions developed from a unique',
    'Responsibilities: Validated and processed insurance policy change request',
    'Responsibilities: Played a vital role in the creation and maintenance of',
    'Project involved in developing a Web-based Inventory. The application allows the clint to',
    'This is an online insurance quoting and policy application, which enables customers to',
    'This Banking application allows customers to access their Bank account through World Wide',
    'Web-based Java application which provides rates for the insurance policy premiums based',
    'Responsibilities: Involved in the Design, Development Phases of quotes, customer service modules.',

    "BP is one of the world's leading integrated oil and gas companies. Like all",
    'Development of financial reporting for one of the largest hospitals, based on clinical variables',
    'IP Device Management and Reporting solution is used to manage fault and performance for SNMP',
    'Port legacy NetExpert C++ application to run on RHEL5.5 platform. Internationalization (i18n)',
    'This is an enterprise server side solution which added service assurancen',
    'Responsibilities: Provide software and network consultation providing networking consultancy and',
    'Virtual DOS Machine in OS is used to support legacy 16 bit application.',
    'Responsibilities: Design and develop and maintenance of Cisco IOS device driver software.',
    'Locomotive Distribution and Assignment system has been primarily supported for strategic planning',
    'Islamic banking is banking or banking activity that is consistent with the principles of',
    'Intellect is a banking product which is an integration of Core System, Lending System, Collateral',
    'Learning Management System is the E-learning based product .The main aim of this product is learning',

]

date_list = ['Jan 1995 - january 1996', 'Jan 1995 TO january 1996', 'Jan 1995 to january 1996', 'AUG 1996 - JAN 1997', 'AUG 1996 TO JAN 1997', 'AUG 1996 to JAN 1997', 'JUL 1997 - nov 1998',
 'JUL 1997 TO nov 1998', 'JUL 1997 to nov 1998', 'SEPT 1998 - Nov 1999', 'SEPT 1998 TO Nov 1999', 'SEPT 1998 to Nov 1999', 'OCT 1999 - September 2000', 'OCT 1999 TO September 2000',
 'OCT 1999 to September 2000', 'NOV 2000 - aug 2001', 'NOV 2000 TO aug 2001', 'NOV 2000 to aug 2001', 'DEC 2001 - april 2002', 'DEC 2001 TO april 2002', 'DEC 2001 to april 2002',
 'JANUARY 2002 - SEPTEMBER 2003', 'JANUARY 2002 TO SEPTEMBER 2003', 'JANUARY 2002 to SEPTEMBER 2003', 'FEBRUARY 2003 - jan 2004', 'FEBRUARY 2003 TO jan 2004', 'FEBRUARY 2003 to jan 2004',
 'MARCH 2004 - FEBRUARY 2005', 'MARCH 2004 TO FEBRUARY 2005', 'MARCH 2004 to FEBRUARY 2005', 'APRIL 2005 - apr 2006', 'APRIL 2005 TO apr 2006', 'APRIL 2005 to apr 2006',
 'MAY 2006 - june 2007', 'MAY 2006 TO june 2007', 'MAY 2006 to june 2007', 'JUNE 2007 - November 2008', 'JUNE 2007 TO November 2008', 'JUNE 2007 to November 2008',
 'JULY 2008 - MAY 2009', 'JULY 2008 TO MAY 2009', 'JULY 2008 to MAY 2009', 'AUGUST 2009 - Nov 2010', 'AUGUST 2009 TO Nov 2010', 'AUGUST 2009 to Nov 2010',
 'SEPTEMBER 2010 - august 2011', 'SEPTEMBER 2010 TO august 2011', 'SEPTEMBER 2010 to august 2011', 'OCTOBER 2011 - october 2012', 'OCTOBER 2011 TO october 2012',
 'OCTOBER 2011 to october 2012', 'jan 2012 - Apr 2013', 'jan 2012 TO Apr 2013', 'jan 2012 to Apr 2013', 'feb 2013 - january 2014', 'feb 2013 TO january 2014',
 'feb 2013 to january 2014', 'mar 2014 - January 2015', 'mar 2014 TO January 2015', 'mar 2014 to January 2015', 'apr 2015 - september 2016', 'apr 2015 TO september 2016',
 'apr 2015 to september 2016', 'aug 2016 - february 2017', 'aug 2016 TO february 2017', 'aug 2016 to february 2017', 'Feb 2017 - MARCH 2018', 'Feb 2017 TO MARCH 2018',
 'Feb 2017 to MARCH 2018', 'Mar 2018 - November 2019', 'Mar 2018 TO November 2019', 'Mar 2018 to November 2019', 'Apr 2019 - JAN 2020', 'Apr 2019 TO JAN 2020', 'Apr 2019 to JAN 2020',
 'Aug 2020 - Mar 2021', 'Aug 2020 TO Mar 2021', 'Aug 2020 to Mar 2021', 'Jul 2021 - FEBRUARY 2022', 'Jul 2021 TO FEBRUARY 2022', 'Jul 2021 to FEBRUARY 2022',
 'Sept 2022 - AUG 2023', 'Sept 2022 TO AUG 2023', 'Sept 2022 to AUG 2023', 'Oct 1995 - JUNE 1996', 'Oct 1995 TO JUNE 1996', 'Oct 1995 to JUNE 1996', 'Oct 1996 - DEC 1997',
 'Oct 1996 TO DEC 1997', 'Oct 1996 to DEC 1997', 'Nov 1997 - may 1998', 'Nov 1997 TO may 1998', 'Nov 1997 to may 1998', 'Dec 1998 - Aug 1999', 'Dec 1998 TO Aug 1999',
 'Dec 1998 to Aug 1999', 'January 1999 - Jul 2000', 'January 1999 TO Jul 2000', 'January 1999 to Jul 2000', 'February 2000 - november 2001', 'February 2000 TO november 2001',
 'February 2000 to november 2001', 'March 2001 - july 2002', 'March 2001 TO july 2002', 'March 2001 to july 2002', 'April 2002 - May 2003', 'April 2002 TO May 2003', 'April 2002 to May 2003',
 'May 2003 - sept 2004', 'May 2003 TO sept 2004', 'May 2003 to sept 2004', 'November 2004 - AUGUST 2005', 'November 2004 TO AUGUST 2005', 'November 2004 to AUGUST 2005',
 'December 2005 - May 2006', 'December 2005 TO May 2006', 'December 2005 to May 2006', 'jul 2006 - june 2007', 'jul 2006 TO june 2007', 'jul 2006 to june 2007', 'sept 2007 - FEB 2008',
 'sept 2007 TO FEB 2008', 'sept 2007 to FEB 2008', 'oct 2008 - NOVEMBER 2009', 'oct 2008 TO NOVEMBER 2009', 'oct 2008 to NOVEMBER 2009', 'nov 2009 - oct 2010',
 'nov 2009 TO oct 2010', 'nov 2009 to oct 2010', 'dec 2010 - Dec 2011', 'dec 2010 TO Dec 2011', 'dec 2010 to Dec 2011', 'january 2011 - APRIL 2012', 'january 2011 TO APRIL 2012',
 'january 2011 to APRIL 2012', 'february 2012 - Jan 2013', 'february 2012 TO Jan 2013', 'february 2012 to Jan 2013', 'march 2013 - nov 2014', 'march 2013 TO nov 2014',
 'march 2013 to nov 2014', 'april 2014 - APR 2015', 'april 2014 TO APR 2015', 'april 2014 to APR 2015', 'jan 2015 - Mar 2016', 'jan 2015 TO Mar 2016', 'jan 2015 to Mar 2016',
 'feb 2016 - march 2017', 'feb 2016 TO march 2017', 'feb 2016 to march 2017', 'mar 2017 - july 2018', 'mar 2017 TO july 2018', 'mar 2017 to july 2018', 'apr 2018 - JUNE 2019',
 'apr 2018 TO JUNE 2019', 'apr 2018 to JUNE 2019', 'aug 2019 - September 2020', 'aug 2019 TO September 2020', 'aug 2019 to September 2020', 'jul 2020 - JUL 2021', 'jul 2020 TO JUL 2021',
 'jul 2020 to JUL 2021', 'sept 2021 - april 2022', 'sept 2021 TO april 2022', 'sept 2021 to april 2022', 'oct 2022 - APR 2023', 'oct 2022 TO APR 2023', 'oct 2022 to APR 2023',
 'nov 1995 - June 1996', 'nov 1995 TO June 1996', 'nov 1995 to June 1996', 'dec 1996 - APRIL 1997', 'dec 1996 TO APRIL 1997', 'dec 1996 to APRIL 1997', 'january 1997 - JULY 1998',
 'january 1997 TO JULY 1998', 'january 1997 to JULY 1998', 'february 1998 - NOVEMBER 1999', 'february 1998 TO NOVEMBER 1999', 'february 1998 to NOVEMBER 1999', 'march 1999 - Apr 2000',
 'march 1999 TO Apr 2000', 'march 1999 to Apr 2000', 'april 2000 - OCTOBER 2001', 'april 2000 TO OCTOBER 2001', 'april 2000 to OCTOBER 2001', 'may 2001 - feb 2002', 'may 2001 TO feb 2002',
 'may 2001 to feb 2002', 'may 2002 - mar 2003', 'may 2002 TO mar 2003', 'may 2002 to mar 2003', 'june 2003 - Oct 2004', 'june 2003 TO Oct 2004', 'june 2003 to Oct 2004',
 'july 2004 - JANUARY 2005', 'july 2004 TO JANUARY 2005', 'july 2004 to JANUARY 2005', 'august 2005 - OCTOBER 2006', 'august 2005 TO OCTOBER 2006', 'august 2005 to OCTOBER 2006',
 'september 2006 - SEPT 2007', 'september 2006 TO SEPT 2007', 'september 2006 to SEPT 2007', 'october 2007 - August 2008', 'october 2007 TO August 2008', 'october 2007 to August 2008',
 'november 2008 - August 2009', 'november 2008 TO August 2009', 'november 2008 to August 2009', 'december 2009 - JANUARY 2010', 'december 2009 TO JANUARY 2010',
 'december 2009 to JANUARY 2010', 'JAN 2010 - Oct 2011', 'JAN 2010 TO Oct 2011', 'JAN 2010 to Oct 2011', 'FEB 2011 - Feb 2012', 'FEB 2011 TO Feb 2012', 'FEB 2011 to Feb 2012',
 'MAR 2012 - OCT 2013', 'MAR 2012 TO OCT 2013', 'MAR 2012 to OCT 2013', 'July 2013 - Aug 2014', 'July 2013 TO Aug 2014', 'July 2013 to Aug 2014', 'August 2014 - April 2015',
 'August 2014 TO April 2015', 'August 2014 to April 2015', 'September 2015 - Dec 2016', 'September 2015 TO Dec 2016', 'September 2015 to Dec 2016', 'October 2016 - jul 2017',
 'October 2016 TO jul 2017', 'October 2016 to jul 2017', 'November 2017 - jul 2018', 'November 2017 TO jul 2018', 'November 2017 to jul 2018', 'Nov 2018 - AUG 2019', 'Nov 2018 TO AUG 2019',
 'Nov 2018 to AUG 2019', 'Dec 2019 - MAR 2020', 'Dec 2019 TO MAR 2020', 'Dec 2019 to MAR 2020', 'January 2020 - Sept 2021', 'January 2020 TO Sept 2021', 'January 2020 to Sept 2021',
 'February 2021 - July 2022', 'February 2021 TO July 2022', 'February 2021 to July 2022', 'APR 2022 - DECEMBER 2023', 'APR 2022 TO DECEMBER 2023', 'APR 2022 to DECEMBER 2023',
 'Jan 1995 - AUGUST 1996', 'Jan 1995 TO AUGUST 1996', 'Jan 1995 to AUGUST 1996', 'Feb 1996 - december 1997', 'Feb 1996 TO december 1997', 'Feb 1996 to december 1997',
 'Mar 1997 - oct 1998', 'Mar 1997 TO oct 1998', 'Mar 1997 to oct 1998', 'JANUARY 1998 - Jul 1999', 'JANUARY 1998 TO Jul 1999', 'JANUARY 1998 to Jul 1999', 'FEBRUARY 1999 - feb 2000',
 'FEBRUARY 1999 TO feb 2000', 'FEBRUARY 1999 to feb 2000', 'MARCH 2000 - SEPT 2001', 'MARCH 2000 TO SEPT 2001', 'MARCH 2000 to SEPT 2001', 'june 2001 - august 2002',
 'june 2001 TO august 2002', 'june 2001 to august 2002', 'july 2002 - December 2003', 'july 2002 TO December 2003', 'july 2002 to December 2003', 'august 2003 - FEB 2004',
 'august 2003 TO FEB 2004', 'august 2003 to FEB 2004', 'FEB 2004 - march 2005', 'FEB 2004 TO march 2005', 'FEB 2004 to march 2005', 'MAR 2005 - DECEMBER 2006',
 'MAR 2005 TO DECEMBER 2006', 'MAR 2005 to DECEMBER 2006', 'APR 2006 - NOV 2007', 'APR 2006 TO NOV 2007', 'APR 2006 to NOV 2007', 'NOVEMBER 2007 - April 2008', 'NOVEMBER 2007 TO April 2008',
 'NOVEMBER 2007 to April 2008', 'DECEMBER 2008 - may 2009', 'DECEMBER 2008 TO may 2009', 'DECEMBER 2008 to may 2009', 'september 2009 - DEC 2010', 'september 2009 TO DEC 2010',
 'september 2009 to DEC 2010', 'october 2010 - February 2011', 'october 2010 TO February 2011', 'october 2010 to February 2011', 'november 2011 - dec 2012', 'november 2011 TO dec 2012',
 'november 2011 to dec 2012', 'december 2012 - mar 2013', 'december 2012 TO mar 2013', 'december 2012 to mar 2013', 'JAN 2013 - SEPTEMBER 2014',
 'JAN 2013 TO SEPTEMBER 2014', 'JAN 2013 to SEPTEMBER 2014', 'OCT 2014 - January 2015', 'OCT 2014 TO January 2015', 'OCT 2014 to January 2015',
 'NOV 2015 - October 2016', 'NOV 2015 TO October 2016', 'NOV 2015 to October 2016', 'DEC 2016 - MAY 2017', 'DEC 2016 TO MAY 2017', 'DEC 2016 to MAY 2017',
 'June 2017 - aug 2018', 'June 2017 TO aug 2018', 'June 2017 to aug 2018', 'March 2018 - Sept 2019', 'March 2018 TO Sept 2019', 'March 2018 to Sept 2019',
 'April 2019 - March 2020', 'April 2019 TO March 2020', 'April 2019 to March 2020', 'May 2020 - dec 2021', 'May 2020 TO dec 2021', 'May 2020 to dec 2021',
 'June 2021 - december 2022', 'June 2021 TO december 2022', 'June 2021 to december 2022', 'July 2022 - March 2023', 'July 2022 TO March 2023', 'July 2022 to March 2023']

data = []

for i, j, k, l, m in zip(companys * 6, job_role * 21, date_list * 70, garbage_data_1 * 25, garbage_data_2 * 25):
    text1 = l + ' ' + 'Project Experience' + ' ' + 'Client:' + ' ' + i + ' ' + 'Role:' + ' ' + j + ' ' + k + ' ' + m
    lenofcomp = text1.find(i)
    posofcomp = lenofcomp + len(i)

    lenofrole = text1[posofcomp:].find(j) + posofcomp
    posofrole = lenofrole + len(j)

    comp_pos = (lenofcomp, posofcomp, 'COMPANY')
    role_pos = (lenofrole, posofrole, 'ROLE')
    data1 = (text1, {"entities": [comp_pos, role_pos]})
    data.append(data1)

    text2 = l + ' ' + 'PROJECT EXPERIENCE' + ' ' + 'Client:' + ' ' + i + ' ' + 'Designation:' + ' ' + j + ' ' + k + ' ' + m
    lenofcomp = text2.find(i)
    posofcomp = lenofcomp + len(i)

    lenofrole = text2[posofcomp:].find(j) + posofcomp
    posofrole = lenofrole + len(j)

    comp_pos = (lenofcomp, posofcomp, 'COMPANY')
    role_pos = (lenofrole, posofrole, 'ROLE')
    data2 = (text2, {"entities": [comp_pos, role_pos]})
    data.append(data2)

    text3 = l + ' ' + 'WORK EXPERIENCE' + ' ' + 'Company Name:' + ' ' + i + ' ' + 'ROLE' + ' ' + j + ' ' + k + ' ' + m
    lenofcomp = text3.find(i)
    posofcomp = lenofcomp + len(i)

    lenofrole = text3[posofcomp:].find(j) + posofcomp
    posofrole = lenofrole + len(j)

    comp_pos = (lenofcomp, posofcomp, 'COMPANY')
    role_pos = (lenofrole, posofrole, 'ROLE')
    data3 = (text3, {"entities": [comp_pos, role_pos]})
    data.append(data3)

    text4 = l + ' ' + 'PROFESSIONAL EXPERIENCE' + ' ' + i + ' ' + 'Role' + ' ' + j + ' ' + k + ' ' + m
    lenofcomp = text4.find(i)
    posofcomp = lenofcomp + len(i)

    lenofrole = text4[posofcomp:].find(j) + posofcomp
    posofrole = lenofrole + len(j)

    comp_pos = (lenofcomp, posofcomp, 'COMPANY')
    role_pos = (lenofrole, posofrole, 'ROLE')
    data4 = (text4, {"entities": [comp_pos, role_pos]})
    data.append(data4)

    text5 = l + ' ' + 'Professional Work Experience' + ' ' + 'Company Name:' + ' ' + i + ' ' + 'Position' + ' ' + j + ' ' + k + ' ' + m
    lenofcomp = text5.find(i)
    posofcomp = lenofcomp + len(i)

    lenofrole = text5[posofcomp:].find(j) + posofcomp
    posofrole = lenofrole + len(j)

    comp_pos = (lenofcomp, posofcomp, 'COMPANY')
    role_pos = (lenofrole, posofrole, 'ROLE')
    data5 = (text5, {"entities": [comp_pos, role_pos]})
    data.append(data5)

    text6 = l + ' ' + 'PROFESSIONAL WORK EXPERIENCE' + ' ' + i + ' ' + j + ' ' + k + ' ' + m
    lenofcomp = text6.find(i)
    posofcomp = lenofcomp + len(i)

    lenofrole = text6[posofcomp:].find(j) + posofcomp
    posofrole = lenofrole + len(j)

    comp_pos = (lenofcomp, posofcomp, 'COMPANY')
    role_pos = (lenofrole, posofrole, 'ROLE')
    data6 = (text6, {"entities": [comp_pos, role_pos]})
    data.append(data6)

    text7 = l + ' ' + i + ' ' + j + ' ' + k + ' ' + m
    lenofcomp = text7.find(i)
    posofcomp = lenofcomp + len(i)

    lenofrole = text7[posofcomp:].find(j) + posofcomp
    posofrole = lenofrole + len(j)

    comp_pos = (lenofcomp, posofcomp, 'COMPANY')
    role_pos = (lenofrole, posofrole, 'ROLE')
    data7 = (text7, {"entities": [comp_pos, role_pos]})
    data.append(data7)

    text8 = l + ' ' + 'Client:' + ' ' + i + ' ' + 'Role:' + ' ' + j + ' ' + k + ' ' + m
    lenofcomp = text8.find(i)
    posofcomp = lenofcomp + len(i)

    lenofrole = text8[posofcomp:].find(j) + posofcomp
    posofrole = lenofrole + len(j)

    comp_pos = (lenofcomp, posofcomp, 'COMPANY')
    role_pos = (lenofrole, posofrole, 'ROLE')
    data8 = (text8, {"entities": [comp_pos, role_pos]})
    data.append(data8)

    text9 = l + ' ' + 'Client:' + ' ' + i + ' ' + 'Designation:' + ' ' + j + ' ' + k + ' ' + m
    lenofcomp = text9.find(i)
    posofcomp = lenofcomp + len(i)

    lenofrole = text9[posofcomp:].find(j) + posofcomp
    posofrole = lenofrole + len(j)

    comp_pos = (lenofcomp, posofcomp, 'COMPANY')
    role_pos = (lenofrole, posofrole, 'ROLE')
    data9 = (text9, {"entities": [comp_pos, role_pos]})
    data.append(data9)

    text10 = l + ' ' + 'Company Name:' + ' ' + i + ' ' + 'ROLE' + ' ' + j + ' ' + k + ' ' + m
    lenofcomp = text10.find(i)
    posofcomp = lenofcomp + len(i)

    lenofrole = text10[posofcomp:].find(j) + posofcomp
    posofrole = lenofrole + len(j)

    comp_pos = (lenofcomp, posofcomp, 'COMPANY')
    role_pos = (lenofrole, posofrole, 'ROLE')
    data10 = (text10, {"entities": [comp_pos, role_pos]})
    data.append(data10)

    text11 = l + ' ' + i + ' ' + 'Role' + ' ' + j + ' ' + k + ' ' + m
    lenofcomp = text11.find(i)
    posofcomp = lenofcomp + len(i)

    lenofrole = text11[posofcomp:].find(j) + posofcomp
    posofrole = lenofrole + len(j)

    comp_pos = (lenofcomp, posofcomp, 'COMPANY')
    role_pos = (lenofrole, posofrole, 'ROLE')
    data11 = (text11, {"entities": [comp_pos, role_pos]})
    data.append(data11)

    text12 = l + ' ' + j + ' ' + k + ' ' + i + ' ' + m

    lenofrole = text12.find(j)
    posofrole = lenofrole + len(j)

    lenofcomp = text12[posofrole:].find(i) + posofrole
    posofcomp = lenofcomp + len(i)

    comp_pos = (lenofcomp, posofcomp, 'COMPANY')
    role_pos = (lenofrole, posofrole, 'ROLE')
    data12 = (text12, {"entities": [role_pos, comp_pos]})
    data.append(data12)

    text13 = l + ' ' + 'Role:' + ' ' + j + ' ' + 'Duration:' + ' ' + k + ' ' + i + ' ' + m
    lenofrole = text13.find(j)
    posofrole = lenofrole + len(j)

    lenofcomp = text13[posofrole:].find(i) + posofrole
    posofcomp = lenofcomp + len(i)

    comp_pos = (lenofcomp, posofcomp, 'COMPANY')
    role_pos = (lenofrole, posofrole, 'ROLE')
    data13 = (text13, {"entities": [role_pos, comp_pos]})
    data.append(data13)

    text14 = l + ' ' + 'Role:' + ' ' + j + ' ' + k + ' ' + 'Client:' + i + ' ' + m
    lenofrole = text14.find(j)
    posofrole = lenofrole + len(j)

    lenofcomp = text14[posofrole:].find(i) + posofrole
    posofcomp = lenofcomp + len(i)

    comp_pos = (lenofcomp, posofcomp, 'COMPANY')
    role_pos = (lenofrole, posofrole, 'ROLE')
    data14 = (text14, {"entities": [role_pos, comp_pos]})
    data.append(data14)

import random
random.shuffle(data)

train_data = data[0:int(len(data)*.8)]
test_data = data[int(len(data)*.8):]

train_data = train_data[0:10]
test_data = test_data[0:5]


# Convert to train.spacy and dev.spacy

import pandas as pd
import os
from tqdm import tqdm
import spacy
from spacy.tokens import DocBin

nlp = spacy.load("en_core_web_sm") # load other spacy model
db = DocBin() # create a DocBin object
for text, annot in tqdm(train_data): # data in previous format
    doc = nlp.make_doc(text) # create doc object from text
    ents = []
    for start, end, label in annot["entities"]: # add character indexes
        span = doc.char_span(start, end, label=label, alignment_mode="contract")
        if span is None:
            print("Skipping entity")
        else:
            ents.append(span)

    doc.ents = ents # label the text with the ents
    db.add(doc)
# os.chdir(r'C:\XSeed_ML\XSeed_Exp_Code\corpus')
db.to_disk(r".\corpus\train.spacy") # save the docbin object

db = DocBin() # create a DocBin object
for text, annot in tqdm(test_data): # data in previous format
    doc = nlp.make_doc(text) # create doc object from text
    ents = []
    for start, end, label in annot["entities"]: # add character indexes
        span = doc.char_span(start, end, label=label, alignment_mode="contract")
        if span is None:
            print("Skipping entity")
        else:
            ents.append(span)
    doc.ents = ents # label the text with the ents
    db.add(doc)
# os.chdir(r'C:\XSeed_ML\XSeed_Exp_Code\corpus')
db.to_disk(".\corpus\dev.spacy") # save the docbin object