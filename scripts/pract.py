# filename = secure_filename(rec_data["name"])
filename = rec_data["file_path"]
print(filename)
if filename != '':
    file_ext = os.path.splitext(filename)[1]
    print(file_ext)

    if file_ext not in app.config['UPLOAD_EXTENSIONS']:
        pass
        # return jsonify(dict({"error": "Unknown file extension"}))

    txt = filetoText(filename, file_ext)

    educations = extract_education_data(txt)
    print(educations)
    doc_edu = nlp_education(educations)  # input sample t
    Edu_Ent = [(ent.label_, ent.text) for ent in doc_edu.ents]
    Edu_Ent.append(("EOL", "EOL"))
    for lbl, t in Edu_Ent:
        print(lbl, t)
    res = accepts(dfa, 0, {1}, Edu_Ent)
    print('res:', res)
    edu_d_l = []
    if (res):
        for i in res:
            if (len(i) == 3):
                if (i[0][0] == 'QUALIFICATION' and i[1][0] == 'UNIVERSITY' and i[2][0] == 'DATE'):
                    dd = {"School": i[1][1], "Course": i[0][1], "StartDate": "StartDate", "EndDate": "EndDate"}
                    edu_d_l.append(dd)
                # print(dd)
                if (i[1][0] == 'QUALIFICATION' and i[0][0] == 'UNIVERSITY'):
                    dd = {"School": i[0][1], "Course": i[1][1], "StartDate": "StartDate", "EndDate": "EndDate"}
                    # print(dd)
                    edu_d_l.append(dd)

    # extract employment data
    """                /* Employement Info */

            model.Positions = new List<Position>();
            foreach (var position in parsedResume.Positions)
            {
                Position positionModel = new Position();
                positionModel.Company = position.Company;
                positionModel.Summary = position.Summary;
                positionModel.StartDate = position.StartDate;
                positionModel.EndDate = position.EndDate;
                model.Positions.Add(positionModel);
            }
"""
    dt = extractDateText.getDatestext(txt)
    ExtractedDataExpereince = []
    for d in dt:
        print(f"date: {d} {d['period'].split('--')}")
        doc = nlp_experience(d['text'])
        print(type(doc))
        dd = dict()

        company_list = [ent.text for ent in doc.ents if (ent.label_ == "COMPANY")]
        print(f"company names:----  {company_list}")
        role_list = [ent.text for ent in doc.ents if (ent.label_ == "ROLE")]
        print(f"roles :----  {role_list}")

        dd["Company"] = None
        if (len(company_list) > 0):
            dd["Company"] = company_list[0]

        dd["Summary"] = None  # add job role for time being
        if (len(role_list) > 0):
            dd["Summary"] = [(role_list[0])]

        psl = d['period'].split('--')
        if (len(psl) == 0):
            dd["StartDate"] = None
            dd["EndDate"] = None
        if (len(psl) == 1):
            dd["StartDate"] = psl[0]
            dd["EndDate"] = None
        if (len(psl) == 2):
            dd["StartDate"] = psl[0]
            dd["EndDate"] = psl[1]

        ExtractedDataExpereince.append(dd)

    # extract skills
    skills_dict = extractSkills(txt)

    print(f"{repr(skills_dict)} {type(skills_dict)}")
    Skills = None
    if (skills_dict):
        print("Skills found")
        Skills = getskillsList(skills_dict)

    # create response
    ParsedResume = getResposeDataFields('output.json')
    ParsedResume["Skills"] = Skills

    EmailPhone = getEmailPhonefromText(txt)
    """
    EmailAddress
    Phone
    Mobile
    Location

    """
    """public List<Education> Educations { get; set; }
        --------------Education--------------------------
        public string School { get; set; }
        public string Course { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }"""
    ParsedResume['Educations'] = edu_d_l
    print(edu_d_l)
    ParsedResume["EmailAddress"] = None
    if (len(EmailPhone["Email"]) > 0):
        ParsedResume["EmailAddress"] = EmailPhone["Email"][0]

    ParsedResume["Phone"] = None
    if (len(EmailPhone["PhoneNumber"]) > 0):
        ParsedResume["Phone"] = EmailPhone["PhoneNumber"][0]

    ParsedResume["Mobile"] = None
    if (len(EmailPhone["PhoneNumber"]) > 0):
        ParsedResume["Mobile"] = EmailPhone["PhoneNumber"][0]

    ParsedResume["FirstName"] = None  # need to be changed
    ParsedResume["LastName"] = None  # need to be changed
    ParsedResume["MiddleName"] = None  # need to be changed
    # ParsedResume["Mobile"]=None #need to be changed
    ParsedResume["PrimaryEmail"] = ParsedResume["EmailAddress"]  # need to be changed
    ParsedResume["ProfileSummary"] = None

    name = getNameSummaryfromText(txt)
    if (name):
        lst = name.split()
        if (len(lst) > 3):
            pass
        if (len(lst) == 3):
            ParsedResume["FirstName"] = lst[0]  # need to be changed
            ParsedResume["LastName"] = lst[2]  # need to be changed
            ParsedResume["MiddleName"] = lst[1]  # need to be changed
        if (len(lst) == 2):
            ParsedResume["FirstName"] = lst[0]  # need to be changed
            ParsedResume["LastName"] = lst[1]  # need to be changed
        #    ParsedResume["MiddleName"]=lst[2] #need to be changed
        if (len(lst) == 1):
            ParsedResume["FirstName"] = lst[0]  # need to be changed
        #    ParsedResume["LastName"]=lst[1] #need to be changed
        #    ParsedResume["MiddleName"]=lst[2] #need to be changed

    ParsedResume["EmployementHistory"] = ExtractedDataExpereince
    ParsedResume["Positions"] = ExtractedDataExpereince
    json_reponse = dict()
    json_reponse["Status"] = "200"
    json_reponse["Data"] = {"ParsedResume": ParsedResume}
    json_reponse["Message"] = "Success"

# # return jsonify(ExtractedDataExpereince)
# return jsonify(json_reponse)
