from spacy.cli.train import train
from spacy.cli.evaluate import evaluate
import mlflow
# import os
# import warnings
# import sys
import numpy as np
from sklearn.model_selection import ParameterGrid
import json
import pickle
import dvc.api

# MLFlow tracking uri
# os.environ['MLFLOW_TRACKING_URI'] = 'http://localhost:8000/'

# Get DVC remote url

cmpny_path = r"assets\Company Data.xlsx"
job_path = r"assets\job role.xlsx"
repo = r"."
version = "v1"

cmpny_url = dvc.api.get_url(
    path=cmpny_path,
    repo=repo,
    rev=version
)
job_url = dvc.api.get_url(
    path=job_path,
    repo=repo,
    rev=version
)

# Pickle file read
pickle_out = open(".\Exp.pickle", "rb")
c = pickle.load(pickle_out)
pickle_out.close()
experiment_name = "Experiment_" + str(c)
c = c + 1
pickle_out = open(".\Exp.pickle", "wb")
pickle.dump(c, pickle_out)
pickle_out.close()

# Experiment for MLFlow
# os.environ['MLFLOW_TRACKING_URI'] = 'postgresql://mlflow:mlflow@localhost/xseed_exp_db'
# mlflow.set_tracking_uri('postgresql://mlflow:mlflow@localhost/xseed_exp_db')
experiment_id = mlflow.create_experiment(experiment_name)
# mlflow.set_experiment(experiment_name)
print(experiment_id)
experiment = mlflow.get_experiment(experiment_id)
print("Name: {}".format(experiment.name))
print("Experiment_id: {}".format(experiment.experiment_id))
print("Artifact Location: {}".format(experiment.artifact_location))
print("Tags: {}".format(experiment.tags))
print("Lifecycle_stage: {}".format(experiment.lifecycle_stage))

param_grid = {"nlp.batch_size": [100,110], "training.dropout": [0.1]}
parameters = list(ParameterGrid(param_grid))
# train("./config.cfg", overrides={"paths.train": "./train.spacy", "paths.dev": "./dev.spacy",'output':'myout'}
count = 0
for params in parameters:
    with mlflow.start_run(experiment_id=experiment_id) as run:
        # print(params)
        count += 1
        print(f"Count is: {count}")
        t = dict()
        for k in params:
           mlflow.log_param(k,params[k])
           t[k]=params[k]
        d=dict({"paths.train": "./corpus/train.spacy", "paths.dev": "./corpus/dev.spacy"})
        d.update(t)
        print(d)
        train(config_path="./configs/config.cfg",output_path='./training/'+str(experiment_name)+'/'+str(count),overrides=d)
        evaluate(model='./training/' + str(experiment_name) + '/' + str(count)+'/model-best', data_path = './corpus/dev.spacy', output='./training/'+str(experiment_name)+'/'+str(count)+'/metrics.json')
        with open('./training/'+str(experiment_name)+'/'+str(count)+'/metrics.json') as f:
            data=json.load(f)
            for k in data:
                print(k,data[k])
                if(k == 'ents_per_type'):
                 for kk in data[k]:
                  print(kk,data[k][kk])
                  mlflow.log_metric(kk+'_'+'p',data[k][kk]['p'])
                  mlflow.log_metric(kk+'_'+'r',data[k][kk]['r'])
                  mlflow.log_metric(kk+'_'+'f',data[k][kk]['f'])
                else:
                 mlflow.log_metric(k, data[k])
        mlflow.log_artifacts('./training/'+str(experiment_name)+'/'+str(count))
        mlflow.log_param("data_company_url", cmpny_url)
        mlflow.log_param("data_job_role_url", job_url)
        mlflow.log_param("data_version", version)